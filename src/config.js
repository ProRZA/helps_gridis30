export const config = {
    svgns: "http://www.w3.org/2000/svg",
    OFFSET_FROM_LEFT: 40,
    // OFFSET_FROM_BOTTOM: 0,
    colorMainLine: '#C1C1C1',
    colorMinorLine: '#FFBFDF',
    widthMainLine: 2,
    widthMinorLine: 0.5,
    logRange: [0, 0.301029995663981, 0.477121254719662, 0.602059991327962, 0.698970004336019, 0.778151250383644, 0.845098040014257, 0.903089986991944, 0.954242509439325],
    redBorder: '#8f15dc',
    greenBorder: '#659065',
    blueBorder: '#45b2ff',
    optionsProject: {
        title: {
            projectName: 'Карта селективности',
            align: "middle",
            offset: 20
        },
        nameAxis: {
            axisX: "Ток, А",
            axisY: "Время, с",
            offsetTopAxisX: 0,
            offsetLeftAxisY: 0
        },
        legend: {
            columns: 0,
            height: 0
        }
    },
    isShowHelpers: {
        title: false,
        axis: false,
        legend: false
    },
    placeDrawLegend: [
        { x: 0, y: 0 },
        { x: 0, y: 1 },
        { x: 1, y: 0 },
        { x: 1, y: 1 },
        { x: 0, y: 2 },
        { x: 1, y: 2 },
        { x: 0, y: 3 },
        { x: 1, y: 3 },
        { x: 2, y: 0 },
        { x: 2, y: 1 },
        { x: 2, y: 2 },
        { x: 2, y: 3 }
    ]
}