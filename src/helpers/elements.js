import { config } from '../config';

const deleteElement = id => {
    const element = document.getElementById(id);
    if (element) element.remove();
}

const createCircle = ({ x, y, radius, colorFill }) => {
    const circle = document.createElementNS(config.svgns, 'ellipse');
    circle.setAttributeNS(null, 'cx', x);
    circle.setAttributeNS(null, 'cy', y);
    circle.setAttributeNS(null, 'rx', radius);
    circle.setAttributeNS(null, 'ry', radius);
    circle.setAttributeNS(null, 'fill', colorFill);
    return circle;
}

const createWrapper = ({ id = "" }) => {
    if (id.length > 0) deleteElement(id);
    const g = document.createElementNS(config.svgns, 'g');
    g.setAttribute('id', id);
    return g;
}

const createLine = ({ x1, y1, x2, y2,
    options = {
        'stroke-width': 1,
        stroke: '#000000',
        'stroke-dasharray':0
    }
}) => {
    const line = document.createElementNS(config.svgns, 'line');
    line.setAttributeNS(null, 'x1', x1);
    line.setAttributeNS(null, 'y1', y1);
    line.setAttributeNS(null, 'x2', x2);
    line.setAttributeNS(null, 'y2', y2);
    Object.keys(options).forEach(attr => line.setAttributeNS(null, attr, options[attr]));
    return line;
}

const createRect = ({ x, y, width, height, fill, id = "" }) => {
    if (id.length > 0) deleteElement(id);
    const rect = document.createElementNS(config.svgns, 'rect');
    rect.setAttribute("id", id);
    rect.setAttributeNS(null, 'x', x);
    rect.setAttributeNS(null, 'y', y);
    rect.setAttributeNS(null, 'width', width);
    rect.setAttributeNS(null, 'height', height);
    rect.setAttribute('fill', fill);
    return rect;
}

const createText = ({ x, y, text, options = {}, id = "", className = "" }) => {
    if (id.length > 0) deleteElement(id);
    const txt = document.createElementNS(config.svgns, 'text');
    txt.setAttribute('id', id);
    txt.setAttributeNS(null, 'x', x);
    txt.setAttributeNS(null, 'y', y);
    txt.textContent = text;
    if (className.length > 0) txt.classList.add(className)
    Object.keys(options).forEach(attr => txt.setAttributeNS(null, attr, options[attr]));
    return txt;
}

const alignText = ({ txt, align = "left", viewbox, offset = 0 }) => {
    const size = txt.getBoundingClientRect();
    switch (align) {
        case 'left':
            txt.setAttributeNS(null, 'x', viewbox.left + offset);
            break;
        case 'middle':
            txt.setAttributeNS(null, 'x', viewbox.left + (viewbox.right - viewbox.left) * 0.5);
            txt.classList.add('middle');
            break;
        case 'right':
            txt.setAttributeNS(null, 'x', viewbox.right - offset - size.width);
    }
    txt.setAttributeNS(null, 'y', viewbox.top - size.height * 0.5);
}

const createPointer = basePoint => {
    const { x, y, color } = basePoint;

    const circle = createCircle({ x, y, radius: 3, colorFill: color });
    const line1 = createLine({ x1: x - 10, y1: y - 10, x2: x + 10, y2: y + 10 });
    const line2 = createLine({ x1: x - 10, y1: y + 10, x2: x + 10, y2: y - 10 });
    const g = createWrapper({ id: 'pointer' });
    g.style.transformOrigin = `${x}px ${y}px`;
    g.append(circle, line1, line2);
    return g;
}

const createSlice = params => {
    const { x, top, bottom } = params;
    const line = createLine({ x1: x, y1: top, x2: x, y2: bottom, options: {
        'stroke-dasharray': 4,
        'stroke-width': 2,
        stroke: '#ff9900'
    } });
    const g = createWrapper({ id: 'pointer' });
    g.append(line);
    return g;
}

export { createCircle, createWrapper, createLine, createRect, createText, alignText, createPointer, createSlice };