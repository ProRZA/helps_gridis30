const aRound = v => (Math.round(v * 1000) / 1000);

const hypotenuse = (a, b) => (Math.sqrt(Math.pow(b.x - a.x, 2) + Math.pow(b.y - a.y, 2)));

const atg = (a, b) => {
    if (b.x - a.x === 0) b.x++;
    return Math.atan((b.y - a.y) / (b.x - a.x));
}

const getRelativeClientRect = (el, parent) => {
    const rect = el.getBoundingClientRect();
    const parentRect = parent ? parent.getBoundingClientRect() : document.body.getBoundingClientRect();
    return {
        bottom: parentRect.bottom - rect.bottom,
        height: rect.height,
        left: rect.left - parentRect.left,
        right: parentRect.right - rect.right,
        top: rect.top - parentRect.top,
        width: rect.width,
    };
};

const hexToDecColor = colorHex => {
    const hex = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'];
    const val = colorHex.toUpperCase().match(/(\w)(\w)/);
    return hex.indexOf(val[1]) * 16 + hex.indexOf(val[2]);
}

const getRGBA = (colorHex, transparent) => {
    const rgb = colorHex.match(/#(\w{2})(\w{2})(\w{2})/);
    const r = hexToDecColor(rgb[1]);
    const g = hexToDecColor(rgb[2]);
    const b = hexToDecColor(rgb[3]);
    return `rgba(${r}, ${g}, ${b}, ${transparent})`;
}


export { aRound, hypotenuse, atg, getRelativeClientRect, getRGBA };