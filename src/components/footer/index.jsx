import React from "react";

import Grid from "../grid";
import "../../style/footer.sass";

const Footer = () => {
    return (
        <footer className="footer">
            <div className="container">
                <Grid
                    modifier="stretch"
                    columns={[
                        {
                            column: 4,
                            content: (
                                <div className="label">
                                    <div className="logo" />
                                    <div className="signature">
                                        <span className="name">Гридис-КС</span>
                                        <span className="year">2021</span>
                                    </div>
                                </div>
                            )
                        },
                        {
                            column: 4,
                            content: (
                                <div className="contacts">
                                    <span>Контакты</span>
                                    <span>email: admin@pro-rza.ru</span>
                                    <a href="https://gridis.support-desk.ru/form">техподдержка: https://gridis.support-desk.ru/form</a>
                                </div>
                            )
                        },
                        {
                            column: 4,
                            content: (
                                <div className="social">
                                    <span>Мы в соц. сетях</span>
                                    <div className="vk" />
                                </div>
                            )
                        }
                    ]}
                />
            </div>
        </footer>
    );
};

export default Footer;