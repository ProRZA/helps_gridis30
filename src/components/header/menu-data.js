const data = [
    {
        title: 'Панели управления',
        child: [
            {
                title: 'Стандартная характеристика',
                page: 'standart'
            },
            {
                title: 'Автоматы с электронным расцепителем',
                page: 'automat'
            },
            {
                title: 'Автоматы с термомагнитным расцепителем',
                page: 'temp-automat'
            },
            {
                title: 'Модульные автоматы',
                page: 'module'
            },
            {
                title: 'Реле',
                page: 'relay'
            },
            {
                title: 'Предохранители',
                page: 'fuse'
            },
            {
                title: 'Пользовательские кривые',
                page: 'user-curve'
            }
        ]
    },
    {
        title: 'Инструменты',
        child: [
            {
                title: 'Измерения (в разработке)',
                page: 'dev'
            },
            {
                title: 'Вертикальные подписи (в разработке)',
                page: 'dev'
            },
            {
                title: 'Общие настройки',
                page: 'options-project'
            }
        ]
    },
    {
        title: 'Активация и деактивация программы',
        child: [
            {
                title: 'Руководство (в разработке)',
                page: 'dev'
            }
        ]
    },
    {
        title: 'Информационные данные',
        child: [
            {
                title: 'Каталог продукции (в разработке)',
                page: 'dev'
            }
        ]
    }
];

export const getMenu = relativeFolder => {
    return data.map(item => {
        const child = item.child.map(subItem => ({ ...subItem, page: `${relativeFolder}${subItem.page}` }));
        return { ...item, child };
    })
}