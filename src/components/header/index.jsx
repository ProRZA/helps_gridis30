import React, { useState, useContext } from 'react';

import { ScreenContext } from '../../context';

import closeIcon from '../../img/close.svg';

import '../../style/header.sass';
import '../../style/modal.sass';
import { getMenu } from './menu-data';

const Header = () => {
    const context = useContext(ScreenContext);
    const { isMobile } = context;
    const [openMobile, setOpenMobile] = useState(false);
    const menu = getMenu(process.env.SITE_PATH);

    const items = (isMobile = false) => (
        <>
            {menu.map((item, index) => (
                <div
                    key={item.title}
                    className={`menu-item ${isMobile ? "active" : ""}`}
                    tabIndex={index}
                >
                    <div className="menu-item--title">{item.title}</div>
                    <div className="menu-subitem">
                        {item.child.map(subitem => (
                            <a
                                key={subitem.title}
                                href={subitem.page}
                                className="subitem"
                                onClick={() => {}}
                            >
                                {subitem.title}
                            </a>
                        ))}
                    </div> 
                </div>
            ))}
        </>
    )

    return (
        <header className="header">
            <div className="container">
                <div className="header-wrapper">
                    <a href={process.env.SITE_PATH} className="logo" />
                    <menu className="menu">
                        <div
                            className="burger"
                            onClick={() => setOpenMobile(!openMobile)}
                        >
                            <div className="burger-line" />
                            <div className="burger-line" />
                            <div className="burger-line" />
                        </div>
                        {isMobile && openMobile && (
                            <div className="modal">
                                <div className="modal-close-wrapper">
                                    <img
                                        className="modal-icon"
                                        src={closeIcon}
                                        onClick={() => setOpenMobile(false)}
                                    />
                                </div>
                                {items(true)}
                            </div>
                        )}
                        {!isMobile && items()}
                    </menu>
                </div>
            </div>
        </header>
    );
};

export default Header;