import React from 'react';

import '../../style/menu.sass';

import { getMenu } from './menu-data';

const Menu = () => {
    const menu = getMenu(process.env.SITE_PATH);

    return (
        <menu className="menu">
            {menu.map((item,  index) => (
                <div
                    key={item.title}
                    className={`menu-item ${open === item.title ? 'drop-title' : ''}`}
                    tabIndex={index}
                >
                    <div className="menu-item--title">{item.title}</div>
                    <div className="menu-subitem">
                        {item.child.map(subitem => (
                            <a
                                key={subitem.title}
                                href={subitem.page}
                                className="subitem"
                                onClick={() => {}}
                            >
                                {subitem.title}
                            </a>
                        ))}
                    </div> 
                </div>
            ))}
        </menu>
    );
};

export default Menu;