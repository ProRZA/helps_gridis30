import React from 'react';

import Base from '../base';
import Input from '../../input';
import HelpCustom from '../../help-custom';

const Fuse = ({
    baseSettings = {
        globalModifier: '',
        width: 'auto',
        color: "#ffffff",
        brush: "#000000",
        nameCharacter: '',
        panelName: '',
    },
    helpOff = false,
    nameMouseEnterHandler = () => {},
    nameMouseLeaveHandler = () => {},
    helpHandler = () => {},
    parentRef = () => {}
}) => {
    const { nameCharacter } = baseSettings;

    return (
        <Base
            baseSettings={baseSettings}
            nameCharacter={nameCharacter}
            panelName="ПН-2"
            nameMouseEnterHandler={nameMouseEnterHandler}
            nameMouseLeaveHandler={nameMouseLeaveHandler}
            helpOff={helpOff}
            helpHandler={helpHandler}
            parentRef={parentRef}
        >
            <HelpCustom
                helpOff={helpOff}
                helpHandler={() => helpHandler('dataFuse')}
            >
                <Input
                    modifier="fuse"
                    inputModifier="ml-5"
                    labelPosition="left"
                    labelText="In, A"
                    value="31,5"
                    iconType="spin"
                />
            </HelpCustom>
        </Base>
    );
}

export default Fuse;