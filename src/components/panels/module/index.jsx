import React from 'react';

import Base from '../base';
import Section from './section';
import HelpCustom from '../../help-custom';

const Module = ({
    baseSettings = {
        globalModifier: '',
        width: 'auto',
        color: "#ffffff",
        brush: "#000000",
        nameCharacter: '',
        panelName: '',
    },
    helpOff = false,
    helpTopic = 'dataModuleAutomat',
    inSection = "2",
    curveSection = "C",
    nameMouseEnterHandler = () => {},
    nameMouseLeaveHandler = () => {},
    helpHandler = () => {},
    parentRef = () => {}
}) => {
    const { nameCharacter } = baseSettings;

    return (
        <Base
            baseSettings={baseSettings}
            nameCharacter={nameCharacter}
            modifier="module"
            nameMouseEnterHandler={nameMouseEnterHandler}
            nameMouseLeaveHandler={nameMouseLeaveHandler}
            helpOff={helpOff}
            helpHandler={helpHandler}
            parentRef={parentRef}
        >
            <HelpCustom
                modifier="center"
                helpOff={helpOff}
                helpHandler={() => helpHandler(helpTopic)}
            >
                <Section
                    inValue={inSection}
                    curveValue={curveSection}
                />
            </HelpCustom>
        </Base>
    );
}

export default Module;