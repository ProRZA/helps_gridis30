import React from 'react';

import Input from '../../input';

const Section = ({
    inValue = '',
    curveValue = '',
}) => {
    return (
        <>
            <div className="wrapper-module m-20 pr-10">
                <Input
                    inputModifier="ml-5"
                    labelPosition="left"
                    labelText="In, A"
                    iconType="spin"
                    value={inValue}
                />
                <Input
                    inputModifier="ml-5"
                    labelPosition="left"
                    labelText="Кривая"
                    iconType="spin"
                    value={curveValue}
                />
            </div>  
        </>
    );
};

export default Section;