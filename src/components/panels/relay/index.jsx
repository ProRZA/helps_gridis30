import React from 'react';

import Base from '../base';
import HelpCustom from '../../help-custom';

import Section from './section';

const Relay = ({
    baseSettings = {
        globalModifier: '',
        width: 'auto',
        color: "#ffffff",
        brush: "#000000",
        nameCharacter: '',
        panelName: '',
    },
    settings = {},
    helpOff = false,
    nameMouseEnterHandler = () => {},
    nameMouseLeaveHandler = () => {},
    helpHandler = () => {},
    parentRef = () => {}
}) => {
    
    const { nameCharacter } = baseSettings;

    return (
        <Base
            baseSettings={baseSettings}
            nameCharacter={nameCharacter}
            modifier="relay"
            helpOff={helpOff}
            nameMouseEnterHandler={nameMouseEnterHandler}
            nameMouseLeaveHandler={nameMouseLeaveHandler}
            helpHandler={helpHandler}
            parentRef={parentRef}
        >
            <HelpCustom
                modifier="center"
                helpOff={helpOff}
                helpHandler={() => helpHandler('dataRelay')}
            >
                <Section
                    settings={settings}
                />
            </HelpCustom>
        </Base>
    );
}

export default Relay;