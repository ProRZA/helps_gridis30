import React from "react";

import Input from '../../input';

const Section = ({
    settings = {
        inom: {
            labelText: '',
            value: ''
        },
        tc: {
            labelText: '',
            value: ''
        },
        iots: {
            labelText: '',
            value: ''
        }
    },
    off = false,
    changeOff = () => {}
})  => {
    const { inom, tc, iots } = settings;
    return (
        <div className="fields">
            <div className="wrapper-relay">
                <Input
                    inputModifier="ml-5"
                    labelPosition="left"
                    labelText={inom.labelText}
                    value={inom.value}
                />
                <Input
                    inputModifier="ml-5"
                    labelPosition="left"
                    labelText={tc.labelText}
                    value={tc.value}
                />
                <Input
                    inputModifier="ml-5"
                    labelPosition="left"
                    labelText={iots.labelText}
                    value={iots.value}
                />
            </div>
            <Input
                modifier="ml-5 mb-10"
                type="checkbox"
                labelPosition="right"
                labelText="OFF"
                labelModifier="ml-3 vc"
                checked={off}
                setValue={changeOff}
            />
        </div>  
    );
};

export default Section;