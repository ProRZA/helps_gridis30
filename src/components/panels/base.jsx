import React, { useState } from 'react';

import Input from '../input';
import Radio from '../Radio';
import Icon from '../icon';
import Grid from '../grid';
import HelpCustom from '../help-custom';

import "../../style/panel.sass";

const Base = ({
    baseSettings,
    modifier = '',
    isStandart = false,
    helpOff = false,
    helpHandler = () => {},
    nameMouseEnterHandler = () => {},
    nameMouseLeaveHandler = () => {},
    children,
    parentRef = () => {},
}) => {
    const { globalModifier, width, color, brush, nameCharacter, panelName } = baseSettings;
    const [light, setLight] = useState('');
    
    return (
        <div ref={parentRef} className={globalModifier} style={{ width }}>
            <div
                className="h25px flex hc vc bs-1 crs-point"
                style={{ 
                    'backgroundColor': `${brush}`,
                    'color': `${color}`
                }}
            >
                {nameCharacter}
            </div>
            <div className={`base-panel ${modifier}`}>
                <div className="ml-5">{panelName}</div>
                <HelpCustom
                    helpOff={helpOff}
                    helpHandler={() => helpHandler('nameCharacter')}

                >
                     <Input
                        modifier="mlr-10 mt-15"
                        inputModifier="stretch"
                        labelText="Имя характеристики"
                        labelModifier="mb-5 hc"
                        value={nameCharacter}
                        iconType="save"
                    />
                </HelpCustom>
                <HelpCustom
                    helpOff={helpOff}
                    helpHandler={() => helpHandler('classU')}
                >
                    <Radio
                        modifier="mlr-5 mt-15"
                        caption="Приведено к Uср, кВ"
                        activeValue={0.4}
                        radioList={[
                            {
                                modifier: 'direction',
                                text: '0.4',
                                value: 0.4
                            },
                            {
                                modifier: 'direction',
                                text: '6.3',
                                value: 6.3
                            },
                            {
                                modifier: 'direction',
                                text: '10.5',
                                value: 10.5
                            }
                        ]}
                    />
                </HelpCustom>                
                {children}
                <HelpCustom
                    helpOff={helpOff}
                    helpHandler={() => helpHandler('buttons')}
                >
                    <Grid
                        modifier="buttons mt-10"
                        columns={[
                            {
                                column: 2,
                                content: (
                                    <Icon type="simafor" modifier="center-icon"/>
                                )
                            },
                            {
                                modifier: "button-center",
                                column: 4,
                                content: (
                                    <>
                                        {isStandart && (<button className="center">Обновить</button>)}
                                    </>
                                )
                            },
                            {
                                modifier: "button-center",
                                column: 4,
                                content: (
                                    <>
                                        {isStandart && (<button className="center">Очистить</button>)}
                                    </>
                                )
                            },
                            {
                                column: 2,
                                content: (
                                    <Icon type="delete" modifier="center-icon"/>
                                )
                            },
                        ]}
                    />
                </HelpCustom>
                <HelpCustom
                    helpOff={helpOff}
                    helpHandler={() => helpHandler('optionsPanel')}
                >
                    <Grid
                        modifier="vc mt-20 mlr-10 mb-5"
                        columns={[
                            {
                                modifier: "pl-10",
                                column: 8,
                                content: (
                                    <Input
                                        inputModifier="w85px"
                                        labelText="Imax(A)&nbsp;=&nbsp;"
                                        labelPosition="left"
                                        iconType="save"
                                        value="70"
                                    />
                                )
                            },
                            {
                                modifier: "flex h-end",
                                column: 4,
                                content: (
                                    <svg
                                        id="identification"
                                        className="crs-point"
                                        width="55"
                                        height="40"
                                    >
                                        <ellipse cx="27" cy="20" rx="5" ry="5"/>
                                    </svg>
                                )
                            }
                        ]}
                    />
                </HelpCustom>
            </div>
        </div>

        
    );
}

export default Base;