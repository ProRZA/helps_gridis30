import React from 'react';

import Radio from '../radio';

const Network = ({
    modifier = ''
}) => {
    return (
        <div className="flex hc">
            <div className={`${modifier} w285px pb-10`}>
                <div
                    className="h25px flex hc vc bs-1 crs-point"
                    style={{ 
                        'backgroundColor': '#ffffff',
                        'color': '#0000ff'
                    }}
                >
                    Расчетное напряжение
                </div>
                <Radio
                    modifier="mlr-5 mt-15"
                    colorBackground="btn-face"
                    caption="Uср. сети, кВ"
                    activeValue={0.4}
                    radioList={[
                        {
                            modifier: 'direction',
                            text: '0.4',
                            value: 0.4
                        },
                        {
                            modifier: 'direction',
                            text: '6.3',
                            value: 6.3
                        },
                        {
                            modifier: 'direction',
                            text: '10.5',
                            value: 10.5
                        }
                    ]}
                />
            </div>   
        </div>
    );
}

export default Network;