import React, { useState } from 'react';

import Base from '../base';
import Input from '../../input';
import Section from './section';
import HelpCustom from '../../help-custom';

const Automat = ({
    baseSettings = {
        globalModifier: '',
        width: 'auto',
        color: "#ffffff",
        brush: "#000000",
        nameCharacter: '',
        panelName: '',
    },
    helpOff = false,
    lightSectionOnHandler = () => {},
    lightSectionOffHandler = () => {},
    nameMouseEnterHandler = () => {},
    nameMouseLeaveHandler = () => {},
    helpHandler = () => {},
    parentRef = () => {},
}) => {
    const { nameCharacter } = baseSettings;
    const [lightSection, setLightSection] = useState('');
    const [state, setState] = useState({
        L: {
            leftTop: {
                label: 'Ir, о.е.',
                value: '0,6'
            },
            leftBottom: {
                label: 'Tr, c',
                value: '1'
            },
            rightTop: {
                label: 'Ir, A',
                value: '60'
            },
            rightBottom: {
                label: 'При 6 x Ir',
                isStatic: true
            }
        },
        S: {
            leftTop: {
                label: 'Isd, о.е.',
                value: '2,5'
            },
            leftBottom: {
                label: 'Tsd, c',
                value: '0,1'
            },
            rightTop: {
                label: 'Isd, A',
                value: '150'
            },
            rightBottom: {
                isCheckbox: true,
                i2t: false,
                off: false
            }
        },
        I: {
            leftTop: {
                label: 'li, о.е.',
                value: '10'
            },
            rightTop: {
                label: 'li, A',
                value: '1000'
            },
            rightBottom: {
                isCheckbox: true,
                off: false,
                onlyOff: true
            }
        },
        G: {
            leftTop: {
                label: 'Ig, о.е.',
                value: 'A'
            },
            leftBottom: {
                label: 'tg, c',
                value: '0,2'
            },
            rightTop: {
                label: 'Ig, A',
                value: '30'
            },
            rightBottom: {
                isCheckbox: true,
                i2t: false,
                off: false,
            }
        }
    });

    const changeI2t = (v, el) => {
        setState({
            ...state,
            [el]: {
                ...state[el],
                rightBottom: {
                    ...state[el].rightBottom,
                    i2t: v
                }
            }
        });
    }

    const changeOff = (v, el) => {
        const newState = {
            ...state,
            [el]: {
                ...state[el],
                rightBottom: {
                    ...state[el].rightBottom,
                    off: v
                }
            }
        };

        if (newState.S.rightBottom.off && newState.I.rightBottom.off) {
            console.log('низя');
            return;
        }
        setState(newState);
    }

    const lightSectionOn = element => {
        const { section, point, type } = element;
        setLightSection(section);
        lightSectionOnHandler(point, type);
    }

    const lightSectionOff = () => {
        setLightSection('');
        lightSectionOffHandler('');
    }

    return (
        <Base
            baseSettings={baseSettings}
            nameCharacter={nameCharacter}
            modifier="automat"
            nameMouseEnterHandler={nameMouseEnterHandler}
            nameMouseLeaveHandler={nameMouseLeaveHandler}
            helpOff={helpOff}
            helpHandler={helpHandler}
            parentRef={parentRef}
        >
            <HelpCustom
                helpOff={helpOff}
                helpHandler={() => helpHandler('inomAutomat')}
            >
                <Input
                    modifier="flex vc hc mt-20 mb-15"
                    inputModifier="ml-5 w150px"
                    labelText="In, A"
                    labelPosition="left"
                    value="100"
                    iconType="spin"
                />
            </HelpCustom>
            <HelpCustom
                modifier="center"
                helpOff={helpOff}
                helpHandler={() => helpHandler('LAutomat')}
            >
                <Section
                    modifier="mlr-5"
                    sectionName="Перегрузка (L)"
                    inputs={state.L}
                />
            </HelpCustom>
            <HelpCustom
                modifier="center"
                helpOff={helpOff}
                helpHandler={() => helpHandler('SAutomat')}
            >
                <Section
                    modifier="mlr-5"
                    sectionName="КЗ, с выдержкой времени (S)"
                    inputs={state.S}
                    changeDependedHandler={v => changeI2t(v,'S')}
                    changeOffHandler={v => changeOff(v, 'S')}
                    disabled={state.S.rightBottom.off}
                />
            </HelpCustom>
            <HelpCustom
                modifier="center"
                helpOff={helpOff}
                helpHandler={() => helpHandler('IAutomat')}
            >
                <Section
                    modifier={`mlr-5 ${lightSection === 'i' ? 'light-border' : ''}`}
                    sectionName="КЗ, мгновенно (I)"
                    inputs={state.I}
                    changeOffHandler={v => changeOff(v, 'I')}
                    disabled={state.I.rightBottom.off}
                />
            </HelpCustom>
            <HelpCustom
                modifier="center"
                helpOff={helpOff}
                helpHandler={() => helpHandler('GAutomat')}
            >
                <Section
                    modifier={`mlr-5 ${lightSection === 'g' ? 'light-border' : ''}`}
                    sectionName="КЗ на землю (G)"
                    inputs={state.G}
                    changeDependedHandler={v => changeI2t(v,'G')}
                    changeOffHandler={v => changeOff(v, 'G')}
                    disabled={state.G.rightBottom.off}
                />
            </HelpCustom>
        </Base>
    );
}

export default Automat;