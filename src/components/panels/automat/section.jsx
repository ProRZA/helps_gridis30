import React from 'react';

import Input from '../../input';

const Section = ({
    modifier = '',
    sectionName = '',
    inputs = {
        leftTop: {
            label: '',
            value: ''
        },
        leftBottom: {
            label: '',
            value: ''
        },
        rightTop: {
            label: '',
            value: ''
        },
        rightBottom: {
            label: '',
            isStatic: false,
            isCheckbox: false,
            i2t: false,
            off: false,
            onlyOff: true,
        }
    },
    paintInput = false,
    disabled = false,
    mouseEnterHandler = () => {},
    mouseLeaveHandler = () => {},
    changeDependedHandler = () => {},
    changeOffHandler = () => {}
}) => {
    const off = !!inputs.rightBottom.off;
    return (
        <div 
            className={`automat-section ${modifier}`}
            onMouseEnter={mouseEnterHandler}
            onMouseLeave={mouseLeaveHandler}
        >
            <div className="caption mb-10">{sectionName}</div>
            <div className="automat-inputs">
                <div className="">
                    <Input
                        modifier="mb-5"
                        inputModifier={`ml-5 ${paintInput ? 'paint-input' : ''}`}
                        labelModifier="hc"
                        labelPosition="left"
                        labelText={inputs.leftTop?.label}
                        value={off ? "OFF" : inputs.leftTop?.value}
                        iconType="spin"
                        disabled={disabled}
                    />
                    {inputs.leftBottom && (
                        <Input
                            inputModifier={`ml-5 ${paintInput ? 'paint-input' : ''}`}
                            labelModifier="hc"
                            labelPosition="left"
                            labelText={inputs.leftBottom?.label}
                            value={off ? "OFF" : inputs.leftBottom?.value}
                            iconType="spin"
                            disabled={disabled}
                        />
                    )}
                </div>
                <div className="mr-15">
                    <Input
                        modifier="mb-5"
                        inputModifier={`ml-5 ${paintInput ? 'paint-input' : ''}`}
                        labelModifier="hc"
                        labelPosition="left"
                        labelText={inputs.rightTop?.label}
                        value={off ? "OFF" : inputs.rightTop?.value}
                        disabled={disabled}
                    />
                    {inputs.rightBottom.isStatic && (
                        <div className="static-field">{inputs.rightBottom.label}</div>
                    )}
                    {inputs.rightBottom.isCheckbox && (
                        <div className={`wrapper-checked ${inputs.rightBottom.onlyOff ? 'only-off' : ''}`}>
                            {!inputs.rightBottom.onlyOff && (
                                <Input
                                    type="checkbox"
                                    inputModifier={`ml-5 ${paintInput ? 'paint-input' : ''}`}
                                    labelModifier="hc"
                                    labelPosition="left"
                                    labelText='I2t'
                                    checked={inputs.rightBottom.i2t}
                                    disabled={disabled}
                                    setValue={v => changeDependedHandler(v)}
                                />
                            )}
                            <Input
                                type="checkbox"
                                inputModifier={`ml-5 ${paintInput ? 'paint-input' : ''}`}
                                labelModifier="hc"
                                labelPosition="left"
                                labelText='OFF'
                                checked={inputs.rightBottom?.off}
                                setValue={v => changeOffHandler(v)}
                            />
                        </div>
                    )}
                    {!inputs.rightBottom.isStatic && !inputs.rightBottom.isCheckbox && (
                        <Input
                            inputModifier={`ml-5 ${paintInput ? 'paint-input' : ''}`}
                            labelModifier="hc"
                            labelPosition="left"
                            labelText={inputs.rightBottom?.label}
                            value={off ? "OFF" : inputs.rightBottom?.value}
                            disabled={disabled}
                        />
                    )}
                </div>
            </div>            
        </div>
    );
}

export default Section;