import React from 'react';

const Empty = () => {
    return (
        <div className="empty-wrapper">
            <span className="empty-wrapper-title">Данные</span>
        </div>
    );
}

export default Empty;