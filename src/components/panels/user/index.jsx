import React from 'react';

import Base from '../base';
import Section from './section';
import HelpCustom from '../../help-custom';

const User = ({
    baseSettings = {
        globalModifier: '',
        width: 'auto',
        color: "#ffffff",
        brush: "#000000",
        nameCharacter: '',
        panelName: '',
    },
    helpOff = false,
    nameMouseEnterHandler = () => {},
    nameMouseLeaveHandler = () => {},
    helpHandler = () => {},
    parentRef = () => {}
}) => {
    const { nameCharacter } = baseSettings;

    return (
        <Base
            baseSettings={baseSettings}
            nameCharacter={nameCharacter}
            modifier="user"
            nameMouseEnterHandler={nameMouseEnterHandler}
            nameMouseLeaveHandler={nameMouseLeaveHandler}
            helpOff={helpOff}
            helpHandler={helpHandler}
            parentRef={parentRef}
        >
            <HelpCustom
                modifier="center"
                helpOff={helpOff}
                helpHandler={() => helpHandler('dataUser')}
            >
                <Section
                    fileName="Пользовательская кривая.txt"
                    countPoint={7}
                />
            </HelpCustom>
        </Base>
    );
}

export default User;