import React from 'react';

import Input from '../../input';

const  User = ({
    fileName = '',
    countPoint = 0
}) => {
    return (
        <div className="wrapper-user mb-15">
            <Input
                value={fileName}
            />
            <Input
                value={`Кол-во точек: ${countPoint}`}
            />
        </div>
    );
};

export default User;