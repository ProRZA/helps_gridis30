import React, { useState, useEffect, useRef, Children } from 'react';

import '../../style/svg.sass';
import closeIcon from '../../img/close.svg';

import { helps } from '../helps';

const Help = (props) => {
    const { helpBlock, onCloseHelp } = props;

    const leftPanelRef = useRef();
    const contentRef = useRef();
    const [maximum, setMaximum] = useState(false);
    const [helpContent, setHelpContent] = useState(null);
    const [isMobile, setIsMobile] = useState(false);

    useEffect(() => {
        setIsMobile(window.matchMedia(`(max-width: 767px)`).matches);
        window.addEventListener('resize', () => {
            setIsMobile(window.matchMedia(`(max-width: 767px)`).matches);
        });
        return () => {
            window.removeEventListener('resize');
        }
    }, []); 

    useEffect(() => {
        if (helpBlock && helpBlock.topic.length > 0) {
            setHelpContent(helps(helpBlock));
        }

        if (helpBlock.topic.length === 0) {
            setHelpContent(null);
        }
    }, [helpBlock, maximum]);

    useEffect(() => {
        if (helpContent && isMobile) {
            window.scrollTo({
                top: 0,
                behavior: 'smooth'
            });
        }
    }, [helpContent]);
    
    const showLeftPanel = !((isMobile && helpContent) || maximum);

    return (
        <div ref={contentRef} className="container column">
            <div className="flex mt-15">
                {showLeftPanel && (
                    <div ref={leftPanelRef} className="viewport left">
                        <div className="viewport-caption">
                            <div className="viewport-caption-title">Модель</div>
                        </div>
                        <div className="viewport-content">
                            {Children.map(props.children, child => {
                                return React.cloneElement(child);
                            })}
                        </div>
                    </div>
                )}
                {isMobile && helpContent && (
                    <div  className="modal">
                        <div className="modal-close-wrapper">
                            <img
                                className="modal-icon"
                                src={closeIcon}
                                onClick={onCloseHelp}
                            />
                        </div>
                        <div className="modal-content">
                            {helpContent}
                        </div>
                    </div>
                )}
                {!isMobile && helpBlock.topic.length > 0 && helpContent && (
                    <div  className="viewport right">
                    <div className="viewport-caption">
                        <div className="viewport-caption-title">Справка</div>
                        <div
                            className="viewport-caption-helper"
                            onClick={() => setMaximum(!maximum)}
                        >
                            {maximum ? 'Свернуть' : 'Развернуть'}
                        </div>
                    </div>
                    <div className={`viewport-content ${maximum ? 'maximum' : ''}`}>
                        {helpContent}
                    </div>
                </div>
                )}
            </div>
        </div>
    );
}

export default Help;