import React, { useState, useEffect } from 'react';

import Radio from '../../Radio';
import Base from '../base';
import HelpCustom from '../../help-custom';
import { Section, DependSection } from './section';
import '../../../style/panel.sass';

const Standart = ({
    baseSettings = {
        globalModifier: '',
        width: 'auto',
        color: "#ffffff",
        brush: "#000000",
        nameCharacter: '',
    },
    helpOff = false,
    lightSectionOnHandler = () => {},
    lightSectionOffHandler = () => {},
    nameMouseEnterHandler = () => {},
    nameMouseLeaveHandler = () => {},
    drawCurveHandler = () => {},
    helpHandler = () => {},
    parentRef = () => {},
}) => {
    const { nameCharacter } = baseSettings;
    const [lightSection, setLightSection] = useState('');
    const [light, setLight] = useState('');
    const [isDepended, setIsDepended] = useState(false);
    const [step, setStep] = useState('threeSteps');

    useEffect(() => {

    }, []);

    const lightSectionOn = element => {
        const { section, point } = element;
        setLightSection(section);
        lightSectionOnHandler(point);
    }

    const lightSectionOff = () => {
        setLightSection('');
        lightSectionOffHandler('');
    }

    return (
        <Base
            baseSettings={baseSettings}
            nameCharacter={nameCharacter}
            nameMouseEnterHandler={nameMouseEnterHandler}
            nameMouseLeaveHandler={nameMouseLeaveHandler}
            helpHandler={helpHandler}
            parentRef={parentRef}
            helpOff={helpOff}
            isStandart
        >
            <HelpCustom
                helpOff={helpOff}
                helpHandler={() => helpHandler('dependedStandart')}
            >
                <Radio
                    modifier="mlr-5 mt-15"
                    caption="Вид крайней ступени"
                    activeValue={isDepended}
                    radioList={[
                        {
                            text: 'Независимая',
                            value: false
                        },
                        {
                            text: 'Зависимая',
                            value: true
                        }
                    ]}
                    setValue={v => {
                        setIsDepended(v);
                    }}
                />
            </HelpCustom>
            <HelpCustom
                helpOff={helpOff}
                helpHandler={() => helpHandler('stepCount')}
            >
                <Radio
                    modifier="mlr-5 mt-20"
                    caption="Количество ступеней"
                    activeValue={step}
                    radioList={[
                        {
                            text: '1',
                            value: 'oneStep'
                        },
                        {
                            text: '2',
                            value: 'twoSteps'
                        },
                        {
                            text: '3',
                            value: 'threeSteps'
                        }
                    ]}
                    setValue={v => {
                        setStep(v);
                    }}
                />
            </HelpCustom>
            <HelpCustom
                modifier="center"
                helpOff={helpOff}
                helpHandler={() => helpHandler('dataStandart')}
            >
                {isDepended && (
                    <DependSection />
                )}
                {!isDepended && (
                    <Section
                        modifier="mlr-10"
                        sectionName="Третья ступень"
                        labelLeftInput="I>, A"
                        labelRightInput="t>, c"
                        leftInputValue="10"
                        rightInputValue="1000"
                    />
                )}
                {(step !== 'oneStep' || isDepended) && (
                    <Section
                        modifier="mlr-10"
                        sectionName="Вторая ступень"
                        labelLeftInput="I>>, A"
                        labelRightInput="t>>, c"
                        leftInputValue="50"
                        rightInputValue="5"
                        hideRightInput={isDepended && step === 'oneStep'}
                    />
                )}
                {step === 'threeSteps' && (
                    <Section
                        modifier="mlr-10"
                        sectionName="Первая ступень"
                        labelLeftInput="I>>>, A"
                        labelRightInput="t>>>, c"
                        leftInputValue="120"
                        rightInputValue="0.1"
                    />
                )}
            </HelpCustom>
        </Base>
    );
}

export default Standart;