import React, { useState } from 'react';

import Input from '../../input';

const Section = ({
    modifier = '',
    sectionName = '',
    labelLeftInput = '',
    labelRightInput = '',
    leftInputValue = '',
    rightInputValue = '',
    paintInput = false,
    onlyInput = false,
    hideLeftInput = false,
    hideRightInput = false,
    mouseEnterHandler = () => {},
    mouseLeaveHandler = () => {}
}) => {
    return (
        <div 
            className={`${onlyInput ? '' : 'character-section'} ${modifier}`}
            onMouseEnter={mouseEnterHandler}
            onMouseLeave={mouseLeaveHandler}
        >
            <div className="caption">{sectionName}</div>
            <div className="inputs">
                {!hideLeftInput && (
                    <Input
                        inputModifier={paintInput ? 'paint-input' : ''}
                        labelModifier="mb-5 hc"
                        labelText={labelLeftInput}
                        value={leftInputValue}
                    />
                )}
                {!hideRightInput && (
                    <Input
                        inputModifier={paintInput ? 'paint-input' : ''}
                        labelModifier="mb-5 hc"
                        labelText={labelRightInput}
                        value={rightInputValue}
                    />
                )}
            </div>
            
        </div>
    )
}

const DependSection = ({
    modifier = '',
    isKnowK = false,
    changeKHandler = () => {},
    mouseEnterHandler = () => {},
    mouseLeaveHandler = () => {}
}) => {
    const [isK, setIsK] = useState(isKnowK);

    return (
        <div
            className={`${modifier} character-section mlr-10`}
            onMouseEnter={mouseEnterHandler}
            onMouseLeave={mouseLeaveHandler}
        >
            <div className="ta-c">Тип характеристики</div>
            <select className="mt-5">
                <option value="">Нормально инверсная (INV)</option>
                <option value="">Сильно инверсная (VERY)</option>
                <option value="">Чрезвычайно инверсная (EXT)</option>
                <option value="">Зависимая(продол. временем) (L)</option>
                <option value="">RI (RI)</option>
                <option value="">Крутая. Аналог РТВ-1 (Ан. РТВ-1)</option>
                <option value="">Пологая. Аналог РТ-80, РТВ-IV (Ан. РТ-80)</option>
                <option value="">Квадратичная гипербола (I2t)</option>
                <option value="">Биквадратная гипербола (I4t)</option>
            </select>
            <Input
                type="checkbox"
                modifier="vc h-end mt-10"
                inputModifier="width_auto"
                labelPosition="right"
                labelModifier="ml-5"
                labelText="Через коэф. K"
                checked={isK}
                setValue={v => setIsK(v)}
            />
            <Section
                labelLeftInput="Iпуск, А"
                labelRightInput="Коэф. K"
                leftInputValue="10"
                rightInputValue="7.93"
                onlyInput
            />
            {!isK && (
                <Section
                    labelLeftInput="Iсогл., А"
                    labelRightInput="tсогл., с"
                    leftInputValue="30"
                    rightInputValue="50"
                    onlyInput
                />
            )}
        </div>
    )
}

export { Section, DependSection };