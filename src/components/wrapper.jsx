import React, { Children } from 'react';

const Wrapper = (props) => {
    return (
        <div className="wrapper-block">
            {Children.map(props.children, child => {
                return React.cloneElement(child);
            })}
        </div>
    );
}

export default Wrapper;