import React, { useState, useEffect, useRef } from 'react';
import ReactDOM from 'react-dom';

import '../style/popover.sass';

const Popover = ({
    disabled = false,
    width = 'auto',
    title = '',
    position = 'top-popover',
    modifier = '',
    children = React.Children
}) => {
    const popover = useRef();
    const wrapper = useRef();

    const [active, setActive] = useState(false);

    useEffect(() => {
        if (active && wrapper) setPosition();
    });

    const showPopover = () => {
        if (!active) setActive(true);
    }

    const hidePopover = () => {
        if (wrapper) {
            wrapper.current.removeEventListener('mouseleave', hidePopover);
            setActive(false);
        }
    }

    const createPopover = () => {
        const block = (
            <div
                ref={popover}
                className={`popover ${modifier}`}
                style={{ width: width}}
            >
                {title}
                <div className={`${position}`} />
            </div>
        );
        return ReactDOM.createPortal(block, document.getElementById('popover'));
    }

    const setPosition = () => {
        const rectWrapper = wrapper.current.getBoundingClientRect();
        const rectPopover = popover.current.getBoundingClientRect();
        let top;
        let left;
        switch (position) {
            case 'top-popover':
                left = rectWrapper.left + rectWrapper.width / 2 - rectPopover.width / 2;
                top = window.pageYOffset + rectWrapper.top - rectPopover.height - 12;
                break;
            case 'right-popover':
                left = rectWrapper.left + rectWrapper.width  + 5;
                top = window.pageYOffset + rectWrapper.top + rectWrapper.height / 2 - rectPopover.height / 2;
                break;
            case 'bottom-popover':
                left  = rectWrapper.left + rectWrapper.width / 2 -  rectPopover.width / 2;
                top = window.pageYOffset + rectWrapper.top + rectWrapper.height + 5;
                break;
            case 'left-popover':
                left = rectWrapper.left - rectPopover.width - 5;
                top = window.pageYOffset +  rectWrapper.top + rectWrapper.height / 2 - rectPopover.height / 2;
                break;
            default:
                left = 0;
                top = 0;
        }

        popover.current.style.left = `${left}px`;
        popover.current.style.top = `${top}px`;
        wrapper.current.addEventListener('mouseleave', hidePopover);
    }

    return (
        <div
            ref={wrapper}
            onMouseEnter={showPopover}
            onMouseLeave={hidePopover}
        >
            {children}
            {active && createPopover()}
        </div>
    );
}

export default Popover;
