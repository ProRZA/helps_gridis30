import React from 'react';
import closeIcon from '../img/close.svg';

import '../style/modal.sass';

const Modal = (props) => {
    const {
        modifier = '',
        onClose = () => {}
    } = props;

    return (
        <div className="modal">
            <div className="modal-close-wrapper">
                <img
                    className="modal-icon"
                    src={closeIcon}
                    onClick={onClose}
                />
            </div>
            <div className={modifier}>
                {props.children}
            </div>
        </div>
    );
};

export default Modal;