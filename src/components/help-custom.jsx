import React from 'react';

const HelpCustom = (props) => {
    const {
        modifier = '',
        helpOff,
        helpHandler = () => {}
    } = props;

    if (helpOff) {
        return (
            <>
                {props.children}
            </>
        )
    } else {
        return (
            <div className="help-wrapper">
                {props.children}
                <div className={`question ${modifier}`} onClick={helpHandler}>?</div>
            </div>
        );
    };
}

export default HelpCustom;