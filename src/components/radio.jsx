import React from 'react';

import Input from './input';

const Radio = ({
    activeValue = '',
    modifier = '',
    caption = '',
    captionModifier = '',
    radioList = [],
    colorBackground = 'white',
    setValue = () => {}
}) => {
    return (
        <div className={`wrapper top radio-group ${modifier}`}>
            <div className={`caption-radio ${colorBackground} ${captionModifier}`}>{caption}</div>
            <div className={`inputs-wrapper ${colorBackground}`}>
                <div className="inputs">
                    {radioList.map((radio, index) => (
                        <Input
                            key={index}
                            type="radio"
                            modifier={radio.modifier}
                            value={radio.value}
                            checked={radio.value === activeValue}
                            labelText={radio.text}
                            labelModifier="ml-5"
                            labelPosition='right'
                            setValue={() => setValue(radio.value)}
                        />
                    ))}
                </div>
            </div>
        </div>
    );
}

export default Radio;