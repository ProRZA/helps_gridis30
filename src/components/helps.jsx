import React from 'react';

import NameCharacter from '../help-content/base/name-character';
import ClassU from '../help-content/base/classU';
import Buttons from '../help-content/base/buttons';
import OptionsPanel from '../help-content/base/options-panel';

import DependedStandart from '../help-content/standart/depended-standart';
import DataStandart from '../help-content/standart/data';
import StepCount from '../help-content/standart/step-count';

import INomAutomat from '../help-content/automat/inom-automat';
import LAutomat from '../help-content/automat/l-automat';
import SAutomat from '../help-content/automat/s-automat';
import IAutomat from '../help-content/automat/i-automat';
import GAutomat from '../help-content/automat/g-automat';

import DataTempAutomat from '../help-content/temp-automat/data';
import DataModuleAutomat from '../help-content/module/data';
import DataFuse from '../help-content/fuse/data';
import DataRelay from '../help-content/relay/data';
import DataUser from '../help-content/user/data';

import DataCaptionSection from '../help-content/options/caption-section';
import DataAxisSection from '../help-content/options/axis-section';
import DataLegendSection from '../help-content/options/legend-section';


export const helps = params => {
    const topic = topics[params.topic];
    if (typeof topic === 'function') return topic(params);
}

const topics = {
    nameCharacter: () => <NameCharacter/>,
    classU: () => <ClassU/>,
    buttons: props => <Buttons {...props} />,
    optionsPanel: () => <OptionsPanel />,
    dependedStandart: () => <DependedStandart />,
    dataStandart: () => <DataStandart />,
    stepCount: () => <StepCount />,
    inomAutomat: () => <INomAutomat />,
    LAutomat: () => <LAutomat />,
    SAutomat: () => <SAutomat />,
    IAutomat: () => <IAutomat />,
    GAutomat: () => <GAutomat />,
    dataTempAutomat: () => <DataTempAutomat />,
    dataModuleAutomat: () => <DataModuleAutomat />,
    dataFuse: () => <DataFuse />,
    dataRelay: () => <DataRelay />,
    dataUser: () => <DataUser />,
    dataOptionsCaption: () => <DataCaptionSection />,
    dataOptionsAxis: () => <DataAxisSection />,
    dataOptionsLegend: () => <DataLegendSection />
}