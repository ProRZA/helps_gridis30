import React from 'react';

const WrapperCaptionPanel = ({
    modifier = '',
    title = '',
    color = '#000000',
    brush = '#ffffff',
    children = React.Children
}) => {
    return (
        <div className={modifier}>
            <div
                className="h25px flex hc vc bs-1 crs-point"
                style={{ 
                    'backgroundColor': `${brush}`,
                    'color': `${color}`
                }}
            >
                {title}
            </div>
            {children}
        </div>
    )
}

export default WrapperCaptionPanel;