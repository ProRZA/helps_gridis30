import React, { useState } from 'react';

import Input from '../input';
import Radio from '../Radio';

const OptionsCaption = ({
    titleOptions,
    isButtonFontDisabled = false,
    changeAlignHandler = () => {},
    changeOffsetHandler = () => {}
}) => {
    const { align, offset } = titleOptions;
    return  (
        <div className="options-caption">
            <div className="options-caption_value">Настройка имени проекта</div>
            <Radio
                colorBackground="btn-face"
                caption="Выравнивание заголовка"
                activeValue={align}
                radioList={[
                    {
                        modifier: "",
                        value: "left",
                        text: "Слева",
                    },
                    {
                        modifier: "",
                        value: "middle",
                        text: "По центру",
                    },
                    {
                        modifier: "",
                        value: "right",
                        text: "Справа",
                    }
                ]}
                setValue={v => changeAlignHandler(v)}
            />
            <div className="options-caption_down">
                <Input
                    labelText="Отступы по бокам"
                    value={offset}
                    setValue={v => changeOffsetHandler(v)}
                />
                <button disabled={isButtonFontDisabled}>Настройка шрифта</button>
            </div>
    </div>
    );
};

export default OptionsCaption;