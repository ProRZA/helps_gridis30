import React from 'react';

import OptionsCaption from './options-caption';
import OptionsAxis from './options-axis';
import OptionsLegend from './options-legend';
import OptionsButton from './options-button';
import HelpCustom from '../help-custom';
import '../../style/options-project.sass';
import { config } from '../../config';

const OptionsProject = ({
    helpOff = false,
    helpHandler = () => {},
    parentRef = () => {}
}) => {
    const { title, nameAxis, legend } = config.optionsProject;
    return (
        <div className="options">
            <HelpCustom
                modifier="center"
                helpOff={helpOff}
                helpHandler={() => helpHandler('dataOptionsCaption')}
            >
                <OptionsCaption titleOptions={title} />
            </HelpCustom>

            <HelpCustom
                modifier="center"
                helpOff={helpOff}
                helpHandler={() => helpHandler('dataOptionsAxis')}
            >
                <OptionsAxis axisOptions={nameAxis} />
            </HelpCustom>

            <HelpCustom
                modifier="center"
                helpOff={helpOff}
                helpHandler={() => helpHandler('dataOptionsLegend')}
            >
                <OptionsLegend legendOptions={legend} />
            </HelpCustom>

            <HelpCustom
                modifier="center"
                helpOff={helpOff}
                helpHandler={() => helpHandler('dataOptionsButton')}
            >
                <OptionsButton />
            </HelpCustom>
        </div>
    );
};

export default OptionsProject;