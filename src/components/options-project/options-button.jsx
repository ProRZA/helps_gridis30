import React from 'react';

const OptionsButton = ({

}) => {
    return (
        <div className="options-button">
            <div className="options-button_left">
                <button>Сохранить для всех проектов</button>
                <button>Сохранить для текущего проекта</button>
            </div>
            <div className="options-button_right">
                <button>Выйти без сохранения</button>
                <button>Сбросить настройки</button>
            </div>
        </div>
    );
};

export default OptionsButton;