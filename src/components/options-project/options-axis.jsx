import React from 'react';

import Input from '../input';

const OptionsAxis = ({
    axisOptions,
    isButtonFontDisabled = false,
    nameAxisXHandler = () => {},
    nameAxisYHandler = () => {},
    offsetTopXHandler = () => {},
    offsetLeftYHandler = () => {}
}) => {
    const { axisX, axisY, offsetTopAxisX, offsetLeftAxisY } = axisOptions;

    return  (
        <div className="options-axis">
            <div className="options-axis_value">Настройка подписей осей</div>
            <Input
                labelText="Подпись оси X"
                inputModifier="stretch"
                value={axisX}
                setValue={nameAxisXHandler}
            />
            <Input
                modifier="mt-10"
                labelText="Подпись оси Y"
                inputModifier="stretch"
                value={axisY}
                setValue={nameAxisYHandler}
            />
            <div className="options-axis_down">
                <div className="">
                    <Input
                        labelText="Отступ сверху для подписи оси X"
                        value={offsetTopAxisX}
                        setValue={offsetTopXHandler}
                    />
                    <Input
                        modifier="mt-10"
                        labelText="Отступ слева для подписи оси Y"
                        value={offsetLeftAxisY}
                        setValue={offsetLeftYHandler}
                    />
                </div>
                <button disabled={isButtonFontDisabled}>Настройка шрифта</button>
            </div>
    </div>
    );
};

export default OptionsAxis;