import React from 'react';

import Input from '../input';
import Radio from '../Radio';

const OptionsLegend = ({
    legendOptions,
    changeLegendOptionsHandler = () => {},
}) => {
    const { columns, height } = legendOptions;

    return  (
        <div className="options-legend">
            <div className="options-legend_value">Настройка легенды</div>
            <Radio
                colorBackground="btn-face"
                caption="Количество столбцов"
                activeValue={columns}
                radioList={[
                    {
                        modifier: "",
                        value: 0,
                        text: "авто",
                    },
                    {
                        modifier: "",
                        value: 1,
                        text: "1",
                    },
                    {
                        modifier: "",
                        value: 2,
                        text: "2",
                    },
                    {
                        modifier: "",
                        value: 3,
                        text: "3",
                    },
                    {
                        modifier: "",
                        value: 4,
                        text: "4",
                    }
                ]}
                setValue={changeLegendOptionsHandler('columns')}
            />
            <div className="options-legend_down">
                <Input
                    labelText="Высота легенды"
                    value={height}
                    setValue={changeLegendOptionsHandler('height')}
                />
                <button> Настройка шрифта</button>
            </div>
    </div>
    );
};

export default OptionsLegend;