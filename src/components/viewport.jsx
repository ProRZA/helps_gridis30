import React from 'react';

const Viewport = ({
    modifier = '',
    imageModifier = '',
    children,
    signature = '',
    colorBakground = "white",
    isStretsh = false,
    isCenter = true,
}) => {
    return (
        <div className={`flex ${isCenter ? 'hc' : ''} mt-15`}>
            <div className={`shadow ${isStretsh ? 'stretch' : ''}`}>
                <div className={`images ${colorBakground} ${imageModifier}`}>
                    <div className={modifier}>
                        {children}
                    </div>
                    <span className="signature">{signature}</span>
                </div>
            </div>
        </div>
    );
};

export default Viewport;