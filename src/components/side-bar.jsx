import React from 'react';

import Radio from './radio';
import Popover from './popover';
import '../style/sidebar.sass';

const SideBar = ({
    refParent = null,
    children = React.Children
}) => {
    return (
        <div
            ref={refParent}
            className="w285px"
        >
            <div className="wrapper-button">
                <Popover
                    position="left-popover"
                    modifier="popover-window"
                    title={
                        <div>
                            <div className="title">Добавление панелей</div>
                            <div className="content">
                                Нажмите на эту кнопку, чтобы вызвать окно для добавления панели 
                            </div>
                        </div>
                    }
                >
                    <button>Добавить панель</button>
                </Popover>
            </div>
            <div className="wrapper-classu">
                <div className="caption-panel">Расчетное напряжение</div>
                <Popover
                    position="left-popover"
                    modifier="popover-window"
                    title={
                        <div>
                            <div className="title">Выбор класс напряжения</div>
                            <div className="content">
                                Панель "Расчетное напряжение" позволяет выставить базовый класс напряжения сети. 
                                Все уставки защитных элементов в схеме будут пересчитываться по напряжению относительно этого базового класса. 
                            </div>
                        </div>
                    }
                >
                    <Radio
                        modifier="ml-5 mb-5 classu"
                        caption="Uср. сети, кВ"
                        activeValue={0.4}
                        radioList={[
                            {
                                modifier: 'direction',
                                text: '0.4',
                                value: 0.4
                            },
                            {
                                modifier: 'direction',
                                text: '6.3',
                                value: 6.3
                            },
                            {
                                modifier: 'direction',
                                text: '10.5',
                                value: 10.5
                            }
                        ]}
                    />
                </Popover>
            </div>
            {children}
        </div>
    );
}

export default SideBar;