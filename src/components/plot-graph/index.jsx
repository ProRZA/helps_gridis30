import React, { useRef, useEffect, useState, useContext } from 'react';

import Plot from '../../classes/plot';
import { curves } from '../curves/basic-curve';
import { ScreenContext } from '../../context';
import { config } from '../../config';

const PlotGraph = ({
    width  = 0,
    height = 0,
    modifier = '',
    modifierSVG = '',
    showCurve = [],
    signatureX = [],
    signatureY = [],
    minX = 1,
    maxX = 10000,
    minY = 0.01,
    maxY = 10000,
    offsetLeft = 40,
    optionsProject = config.optionsProject,
    isShowHelpers = config.isShowHelpers,
    pointer = [],
    legend = [
        {
            title: 'Стандартная характеристика',
            color: '#a80000'
        }
    ],
    fullGrid = false
}) => {
    const context = useContext(ScreenContext);
    const { isMobile } = context;
    const [plot, setPlot] = useState(new Plot(offsetLeft, optionsProject, legend, isShowHelpers));
    const svgRef = useRef();

    const setSize = () => {
        svgRef.current.innerHTML = '';
        plot.changeOffsetLeft(offsetLeft);

        if (width !== 0 && height !== 0) {
            svgRef.current.style.width = `${width}px`;
            svgRef.current.style.height = `${height}px`;
            plot.changeViewbox({ width, height });
        } else {
            const widthContainer = svgRef.current.getBoundingClientRect().width;
            const newHeight = height !== 0 ? height : widthContainer / 2;
            svgRef.current.style.height = `${newHeight}px`;
            plot.changeViewbox({ width: widthContainer, height: newHeight });
        }
    }

    const draw = () => plot.draw(svgRef.current, signatureX, signatureY);
    
    useEffect(() => {
        plot.changeOptionsX({
            min: minX,
            max: maxX,
            viewScale: 'log'
        });
        plot.changeOptionsY({
            min: minY,
            max: maxY,
            viewScale: 'log'
        });
        plot.clearCurves();
    }, []);

    useEffect(() => {
        plot.fullGrid = fullGrid || isMobile;
    }, [fullGrid]);

    useEffect(() => {
        setSize();
        
        plot.build();
        plot.clearCurves();
        const curves_ = showCurve.map(curveName => curves[curveName]);
        if (curves_.length > 0) plot.addCurve(curves_);
        draw();

        if (pointer.length > 0) {
            pointer.forEach(p => {
                const convertPos = p.pos.map(point => plot.plotToScr(point));
                const obj = getPointer({ ...p, pos: convertPos });
                svgRef.current.append(obj);
            });
        }
    });

    useEffect(() => {
        plot.changeShowHelpers(isShowHelpers);
        draw();
    }, [isShowHelpers]);

    useEffect(() => {
        plot.changeOptionsProject(optionsProject);
        draw();
    }, [optionsProject]);

    return (
        <div className={modifier}>
            <svg className={modifierSVG} ref={svgRef} />
        </div>
    )
}

export default PlotGraph;
