import React,{ useState } from 'react';

import '../style/input.sass';

const Icon = ({
    type = '',
    isButtonPush = false,
    isStatic = true,
    modifier = '',
    disabled = false
}) => {
    const [state, setState] = useState('');

    return (
        <div
            className={`icon ${disabled ? 'disabled' : ''} ${type} ${state} ${modifier}`}
            onMouseEnter={() => setState('onmouse')}
            onMouseLeave={() => setState('')}
            onMouseDown={() => setState('onclick')}
            onMouseUp={() => setState('')}
        />
    );
}

export default Icon;