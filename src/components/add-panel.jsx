import React from 'react';

import '../style/add-panel.sass';

const AddPanel = ({
    active = '',
}) => {
    const buttons = [
        "Стандартные характеристики",
        "Автоматы с электронным расцепителем",
        "Автоматы с термомагнитным расцепителем",
        "Модульные автоматы",
        "Реле",
        "Предохранители",
        "Пользовательские кривые"
    ];

    return (
        <div className="add-panel">
            {buttons.map((panel, index) => (
                <button
                    key={index}
                    className={`add-panel-button ${active === panel ? 'active' : ''}`}
                >
                    {panel}
                </button>
            ))}
        </div>
    );
};

export default AddPanel;