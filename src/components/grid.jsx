import React from 'react';

const CellGrid = ({
    modifier = '',
    column,
    content
}) => {
    const width = 100 * column / 12;
    return (
        <div
            className={modifier}
            style={{ width: `${width}%` }}
        >
            {content}
        </div>
    );
}

const Grid = ({
    columns = [],
    modifier = ''
}) => {
    return (
        <div className={`flex ${modifier}`}>
            {columns.map((cell, index) => (
                <CellGrid key={index} {...cell} />
            ))}
        </div>
    );
}

export default Grid;