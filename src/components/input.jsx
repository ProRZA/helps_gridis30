import React from 'react';

import Icon from './icon';

import '../style/input.sass';

const Input = ({
    type = 'text',
    modifier = '',
    labelModifier,
    inputModifier,
    labelText = '',
    labelPosition = 'top',
    value = '',
    checked = false,
    iconType = '',
    disabled = false,
    setValue = () => {},
    mouseEnterHandler = () => {},
    mouseLeaveHandler = () => {}
}) => {
    const id = Math.random();
    return (
        <div 
            className={`wrapper ${modifier} ${labelPosition}`}
            onMouseEnter={mouseEnterHandler}
            onMouseLeave={mouseLeaveHandler}
        >
            {labelText.length > 0 && (
                <label htmlFor={id} className={labelModifier}>{labelText}</label>
            )}
            <div className="input-wrapper">
                <input
                    id={id}
                    type={type}
                    className={inputModifier}
                    value={value}
                    checked={checked}
                    onChange={v => {
                        if (type === 'checkbox') setValue(!checked);
                        if (type === 'radio') setValue(v);
                        if(type === 'text') setValue(v.target.value)
                    }}
                    disabled={disabled}
                />
                {iconType.length > 0 && <Icon type={iconType} disabled={disabled}/>}
            </div>
        </div>
    );
}

export default Input;