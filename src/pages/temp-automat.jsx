import React, { useState } from 'react';

import AddPanel from '../components/add-panel';
import Module from '../components/panels/module';
import Help from '../components/panels/help';
import Viewport from '../components/viewport';

import '../style/svg.sass';

const TempAutomatPage = () => {
    const [topic, setTopic] = useState('');

    return (
        <div className="container column page">
            <h2 className="main_caption gradient_caption">Панель "Автоматы с термомагнитным расцепителем"</h2>
            
            <div className="content-page">
                <p>Для добавление новой панели «Автоматы с термомагнитным расцепителем» нажмите "Добавить панель", 
                    и в появившемся окне выберите вкладку «Автоматы с термомагнитным расцепителем»</p>
                <Viewport
                    signature="Создание панели"
                >
                    <AddPanel
                        active="Автоматы с термомагнитным расцепителем"
                    />
                </Viewport>
            
                <p className="mt-15">После создания характеристики автомата с термомагнитным расцепителем панель заполнится его исходными данными.
                    Исходные данные у каждого автомата делятся на In (номинальный ток) в амперах, и "Кривая".</p>
                <p>Поле "Кривая" указывает режим автомата: нагретое состояние, холодное состоние.</p>
                <p>Значения меняются через стрелки, находящиеся сбоку.</p>
                <p>Все автоматические выключатели внесены в <a className="link" href="#">Каталог продукции</a>, где указаны основные характеристики автоматов.</p>
                <p className="attention">В примере ниже, представлена панель для производителя КЭАЗ. Для получения справки по конкретному разделу панели, нажмите 
                    иконку со знаком вопроса напротив.</p>
                <p className="attention">Наименование уставок в примере относятся к автоматам производителя КЕАЗ. У иных производителей они могут иметь другие наименования.</p>
                <Help
                    helpBlock={{ topic, panel: 'module' }}
                    onCloseHelp={() => setTopic('')}
                >
                    <Module
                        baseSettings={{
                            globalModifier: "shadow",
                            width: '280px',
                            nameCharacter: "Автомат КЕАЗ: Optimat 100",
                            panelName: "КЕАЗ: Optimat 100(16A - Термомагнитный)",
                            color: "#ffffff",
                            brush: "#a80000",
                        }}
                        helpTopic="dataTempAutomat"
                        inSection="16"
                        curveSection="Нагретое состояние"
                        helpHandler={topic => setTopic(topic)}
                    />
                </Help>
            </div>
        </div>
    );
};

export default TempAutomatPage;