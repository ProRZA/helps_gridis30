import React, { useState } from 'react';

import AddPanel from '../components/add-panel';
import Relay from '../components/panels/relay';
import Help from '../components/panels/help';
import Viewport from '../components/viewport';

import '../style/svg.sass';

const RelayPage = () => {
    const [topic, setTopic] = useState('');

    const settings = {
        inom: {
            labelText: "Icp., A",
            value: '350' 
        },
        tc: {
            labelText: 'tco., c',
            value: '4'
        },
        iots: {
            labelText: 'Iотс., о.е.',
            value: '3'
        }
    }

    return (
        <div className="container column page">
            <h2 className="main_caption gradient_caption">Панель "Реле"</h2>
            
            <div className="content-page">
                <p>Панель "Реле" позволяет производить построение защитных характеристик реле.</p>
                <p>Для добавление новой панели "Панель Реле" нажмите "Добавить панель", и в появившемся окне выберите вкладку "Реле"</p>
                <Viewport
                    signature="Создание панели"
                >
                    <AddPanel
                        active="Реле"
                    />
                </Viewport>
            
                <p className="mt-15">После создания характеристики Реле панель заполнится её исходными данными.
                    Исходные данные у каждого реле делятся на:</p>
                <ul>
                    <li><b>Icp.</b> - ток срабатывания, в амперах.</li>
                    <li><b>tcp.</b> - время выдержки срабатывания, в секундах.</li>
                    <li><b>Iотс.</b> - ток отсечки.</li>
                </ul>
                
                <p>Значения меняются через клавиатуру. У каждого значения имеется допустимый диапазон, в котором они изменяются.</p>
                <p>Все реле внесены в <a className="link" href="#">Каталог продукции</a>, где указаны основные характеристики реле.</p>
                <p className="attention">Уставки реле задаются в первичных значениях.</p>
                <p className="attention">Для получения справки по конкретному разделу панели, нажмите иконку со знаком вопроса напротив.</p>
                <Help
                    helpBlock={{ topic, panel: 'relay' }}
                    onCloseHelp={() => setTopic('')}
                >
                    <Relay
                        baseSettings={{
                            globalModifier: "shadow",
                            width: '280px',
                            nameCharacter: "Реле",
                            panelName: "РТ-81/1, PT-83/1, PT-85/1",
                            color: "#ffffff",
                            brush: "#a80000",
                        }}
                        settings={settings}
                        helpHandler={topic => setTopic(topic)}
                    />
                </Help>
            </div>
        </div>
    );
};

export default RelayPage;