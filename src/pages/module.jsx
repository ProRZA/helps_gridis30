import React, { useState } from 'react';

import AddPanel from '../components/add-panel';
import Module from '../components/panels/module';
import Help from '../components/panels/help';
import Viewport from '../components/viewport';

import '../style/svg.sass';

const ModulePage = () => {
    const [topic, setTopic] = useState('');

    return (
        <div className="container column page">
            <h2 className="main_caption gradient_caption">Панель "Модульные aвтоматы"</h2>
            
            <div className="content-page">
                <p>Панель "Модульные aвтоматы" позволяет производить построение защитных характеристик Модульные aвтоматы.</p>
                <p>Для добавление новой панели «Модульные aвтоматы» нажмите "Добавить панель", 
                    и в появившемся окне выберите вкладку «Модульные aвтоматы»</p>
                <Viewport
                    signature="Создание панели"
                >
                    <AddPanel
                        active="Модульные автоматы"
                    />
                </Viewport>
            
                <p className="mt-15">После создания характеристики модульного автомата панель заполнится его исходными данными.
                    Исходные данные у каждого автомата делятся на In (номинальный ток) в амперах, и "Кривая".</p>
                <p>Поле "Кривая" указывает режим автомата: нагретое состояние, холодное состоние.</p>
                <p>Значения меняются через стрелки, находящиеся сбоку.</p>
                <p>Все автоматические выключатели внесены в <a className="link" href="#">Каталог продукции</a>, где указаны основные характеристики автоматов.</p>
                <p className="attention">В примере ниже, представлена панель для производителя Schneider ELectric. Для получения справки по конкретному разделу панели, нажмите 
                    иконку со знаком вопроса напротив.</p>
                <p className="attention">Наименование уставок в примере относятся к автоматам производителя Schneider ELectric. 
                    У иных производителей они могут иметь другие наименования.</p>
                <Help
                    helpBlock={{ topic, panel: 'module' }}
                    onCloseHelp={() => setTopic('')}
                >
                    <Module
                        baseSettings={{
                            globalModifier: "shadow",
                            width: '280px',
                            nameCharacter: "Автомат Schneider Electric",
                            panelName: "Schneider Electric: Multi9(C120H)",
                            color: "#ffffff",
                            brush: "#a80000",
                        }}
                        helpHandler={topic => setTopic(topic)}
                    />
                </Help>
            </div>
        </div>
    );
};

export default ModulePage;