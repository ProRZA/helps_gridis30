import React, { useState } from 'react';

import '../style/svg.sass';

import Automat from '../components/panels/automat';
import AddPanel from '../components/add-panel';
import Help from '../components/panels/help';
import Section from '../components/panels/automat/section';
import Viewport from '../components/viewport';

const AutomatPage = () => {
    const [topic, setTopic] = useState('');

    return (
        <div className="container column page">
            <h2 className="main_caption gradient_caption">Панель «Автоматы с электронным расцепителем»</h2>
            
            <div className="content-page">
                <p>Панель «Автоматы с электронным расцепителем»  позволяет производить построение защитных характеристик автоматов
                     с электронным расцепителем.</p>
                <p>Для добавление новой панели "Автоматы с электронным расцепителем" нажмите "Добавить панель", 
                    и в появившемся окне выберите вкладку "Автоматы с электорнным расцепителем"</p>
                <Viewport
                    signature="Создание панели"
                >
                    <AddPanel
                        active="Автоматы с электронным расцепителем"
                    />
                </Viewport>
            
                <p className="mt-15">После создания характеристики автомата с электронным расцепителем панель заполнится его исходными данными.
                    Исходные данные у каждого автомата отличаются, но количество полей фиксировано.
                    Если у конкретного автомата нет каких-либо уставок, например, уставок максимальной токовой защиты (на панели это секция "КЗ, с выдержкой времени (S)"), 
                    то данные уставки будут отключены (они неактивны, а в полях прописано значение «OFF»)</p>
                <Viewport
                    signature="КЗ, с выдержкой времени (S). Отключенное состояние"
                    modifier="base-panel automat"
                >
                    <Section
                        modifier="mlr-5"
                        sectionName="КЗ, с выдержкой времени (S)"
                        inputs={{
                            leftTop: {
                                label: 'Isd, о.е.',
                                value: '2,5'
                            },
                            leftBottom: {
                                label: 'Tsd, c',
                                value: '0,1'
                            },
                            rightTop: {
                                label: 'Isd, A',
                                value: '150'
                            },
                            rightBottom: {
                                isCheckbox: true,
                                i2t: false,
                                off: true
                            }
                        }}
                        disabled={true}
                    />
                </Viewport>
                <p className="mt-15">Значения меняются через клавиатуру, либо через стрелки, находящиеся сбоку. Если уставка выражена в буквенных значениях, то управлять ей можно только через стрелки. 
                    У каждого значения есть свои диапазоны, в которых эти значения могут изменяться. 
                    При выходе за пределы диапазона «Гридис-КС» вернет ближайшее допустимое значение уставки.</p>
                    <p>Как правило, уставки задаются в относительных единицах, но бывают исключения. 
                        Все автоматические выключатели внесены в <a className="link" href="#">Каталог продукции</a>, где указаны основные характеристики автоматов.</p>
                <p className="attention">В примере ниже, представлена панель для производителя Schnieder Electric. Для получения справки по конкретному разделу панели, нажмите 
                    иконку со знаком вопроса напротив. Наименование уставок в примере относятся к автоматам производителя Schneider Electric. 
                    У иных производителей они могут иметь другие наименования.</p>
                <Help
                    helpBlock={{ topic, panel: 'Automat' }}
                    onCloseHelp={() => setTopic('')}
                >
                    <Automat
                        baseSettings={{
                            globalModifier: "shadow",
                            width: '280px',
                            nameCharacter: "Автомат Schneider Electric",
                            panelName: "Schneider Electric: 6.0A/E",
                            color: "#ffffff",
                            brush: "#a80000",
                        }}
                        helpHandler={topic => setTopic(topic)}
                    />
                </Help>
            </div>
        </div>
    );
}

export default AutomatPage;