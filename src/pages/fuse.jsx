import React, { useState } from 'react';

import AddPanel from '../components/add-panel';
import Fuse from '../components/panels/fuse';
import Help from '../components/panels/help';
import Viewport from '../components/viewport';

import '../style/svg.sass';

const FusePage = () => {
    const [topic, setTopic] = useState('');

    return (
        <div className="container column page">
            <h2 className="main_caption gradient_caption">Панель "Предохранители"</h2>
            
            <div className="content-page">
                <p>Для добавление новой панели "Панель "Предохранители" нажмите "Добавить панель", и в появившемся окне выберите вкладку "Предохранители"</p>
                <Viewport
                    signature="Создание панели"
                >
                    <AddPanel
                        active="Предохранители"
                    />
                </Viewport>
            
                <p className="mt-15">После создания характеристики предохранителя панель заполнится его исходными данными.</p>
                <p>Значение единственной уставки In меняется через стрелки, находящиеся сбоку.</p>
                <p>Все предохранители внесены в <a className="link" href="#">Каталог продукции</a>.</p>
                <p className="attention">Для получения справки по конкретному разделу панели, нажмите иконку со знаком вопроса напротив.</p>
                <Help
                    helpBlock={{ topic, panel: 'fuse' }}
                    onCloseHelp={() => setTopic('')}
                >
                    <Fuse
                        baseSettings={{
                            globalModifier: "shadow",
                            width: '280px',
                            nameCharacter: "Предохранители",
                            panelName: "ПН2 (Выбор аппатуры 0,4кВ, Беляев А.В.)",
                            color: "#ffffff",
                            brush: "#a80000",
                        }}
                        helpHandler={topic => setTopic(topic)}
                    />
                </Help>
            </div>
        </div>
    );
};

export default FusePage;