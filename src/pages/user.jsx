import React, { useState } from 'react';

import AddPanel from '../components/add-panel';
import User from '../components/panels/user';
import Section from '../components/panels/user/section';
import Help from '../components/panels/help';
import Viewport from '../components/viewport';

import '../style/svg.sass';
import userWindow from '../img/user-window.gif';

const UserPage = () => {
    const [topic, setTopic] = useState('');

    return (
        <div className="container column page">
            <h2 className="main_caption gradient_caption">Панель "Пользовательские кривые"</h2>
            
            <div className="content-page">
                <p>Панель "Пользовательские кривые" позволяет строить пользовательские характеристики по точкам из текстового файла. 
                    Данная панель была встроена в «Гридис-КС» для того, чтобы пользователь мог построить 
                    свою характеристику в случае отсутствия последней в базе данных программы.</p>
                <p>Для добавление новой панели "Панель Пользовательские кривые" нажмите "Добавить панель", 
                    и в появившемся окне выберите вкладку «Пользовательские кривые»</p>
                <Viewport
                    signature="Создание панели"
                >
                    <AddPanel
                        active="Пользовательские кривые"
                    />
                </Viewport>
            
                <p>В появившемся окне выбрать файл. Когда файл будет выбран кнопка "Назначить" станет активной.</p>
                <Viewport
                    signature="Пользовательские базы"
                >
                    <img style={{ width: '100%' }} src={userWindow} />
                </Viewport>
                <p className="attention">Загружаемый файл должен быть расположен в папке "UserBase", которая находится в корневой директории Гридис-КС».</p>
                <p>В случае успешной загрузки файла информационное окно заполнится данными: имя файла и количество точек в файле.</p>
                <Viewport
                    modifier="base-panel user"
                    signature="Загруженный пользовательский файл"
                >
                    <Section
                        fileName="Пользовательская кривая.txt"
                        countPoint={7}
                    />
                </Viewport>
                <p className="attention"> Для получения справки по конкретному разделу панели, нажмите иконку со знаком вопроса напротив.</p>
                <Help
                    helpBlock={{ topic, panel: 'user' }}
                    onCloseHelp={() => setTopic('')}
                >
                    <User
                        baseSettings={{
                            globalModifier: "shadow",
                            width: '280px',
                            nameCharacter: "Пользовательская кривая",
                            panelName: "",
                            color: "#ffffff",
                            brush: "#a80000",
                        }}
                        helpHandler={topic => setTopic(topic)}
                    />
                </Help>
            </div>
        </div>
    );
};

export default UserPage;