import React from "react";

import '../style/home-page.sass';
import { getMenu} from '../components/header/menu-data';

const HomePage = () => {
    const menu = getMenu(process.env.SITE_PATH);
    
    return (
        <main className="home">
            <div className="caption">
                <div>Гридис-КС (3.0)</div>
                <div>Руководство пользователя</div>
            </div>

            <div className="container content">
                {menu.map(item => (
                    <div key={item.title} className="content-group">
                        <div className="content-group-title">{item.title}</div>
                        <ul>
                            {item.child.map(subItem => (
                                <li key={subItem.title}><a href={subItem.page}>{subItem.title}</a></li>
                            ))}
                        </ul>
                    </div>
                ))}
            </div>
        </main>
    );
};

export default HomePage;