import React, { useState } from 'react';

import '../style/svg.sass';

import Standart from '../components/panels/characteristic';
import AddPanel from '../components/add-panel';
import Help from '../components/panels/help';
import Viewport from '../components/viewport';

const StandartPage = () => {
    const [topic, setTopic] = useState('');

    return (
        <div className="container column page">
            <h2 className="main_caption gradient_caption">Панель «Стандартная характеристика»</h2>
            
            <div className="content-page">
                <p>Панель «Стандартные кривые» позволяет производить построение различных защитных характеристик с зависимой или независимой третьей ступенью.</p>
                <p>Для добавление новой панели «Стандартная характеристика» нажмите "Добавить панель", и в появившемся окне выберите вкладку «Стандартная характеристика»</p>
                <Viewport
                    signature="Создание панели"
                >
                    <AddPanel
                        active="Стандартные характеристики"
                    />
                </Viewport>
            </div>
            <p className="attention"> Для получения справки по конкретному разделу панели, нажмите иконку со знаком вопроса напротив.</p>
            <Help
                helpBlock={{ topic, panel: 'Standart' }}
                onCloseHelp={() => setTopic('')}
            >
                <Standart
                    baseSettings={{
                        globalModifier: "shadow",
                        width: '280px',
                        nameCharacter: "Стандартная характеристика",
                        color: "#ffffff",
                        brush: "#a80000",
                    }}
                    helpHandler={topic => setTopic(topic)}
                />
            </Help>
        </div>
    );
}

export default StandartPage;