import React, { useState } from 'react';

import Help from '../components/panels/help';
import OptionsProject from '../components/options-project';

const OptionsProjectPage = () => {
    const [topic, setTopic] = useState('');

    return (
        <div className="container column page">
            <h2 className="main_caption gradient_caption">Общие настройки</h2>
            
            <div className="content-page">
                <p>Окно "Общие настройки" находится в меню "Настройки".</p>
                <p className="attention">Все отступы в окне настроек учитываются в пикселях.</p>
                <p className="attention">Для получения справки по конкретному разделу панели, нажмите иконку со знаком вопроса напротив.</p>

                <Help
                    helpBlock={{ topic, panel: '' }}
                    onCloseHelp={() => setTopic('')}
                >
                    <OptionsProject
                        helpHandler={topic => setTopic(topic)}
                    />
                </Help>
            </div>
        </div>
    );
};

export default OptionsProjectPage;