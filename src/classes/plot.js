import { AxisX, AxisY } from './axis';
import { getPointsOfMarkers, drawMarker } from './markers';
import { config } from '../config';
import { getRGBA } from '../helpers/mathFunc';
import { createLine, createText, alignText, createRect, createWrapper } from '../helpers/elements';

const LEGEND_HEIGHT = 80;
const PADDING_LEGEND = 15;
const MARGIN_CAPTION_LEGEND = 30;
const { placeDrawLegend, svgns } = config;

class Plot {
    constructor(offsetLeft, optionsProject, legend, isShowHelpers) {
        this.curves = [];
        this.viewbox = {};
        this.viewLegend = {};
        this.offsetLeft = 40;
        this.axisX = new AxisX(offsetLeft);
        this.axisY = new AxisY();
        this.fullGrid = false;
        this.legend = legend;
        this.isShowHelpers = isShowHelpers;
        this.optionsProject = optionsProject;
        this.heightLegend = LEGEND_HEIGHT;

        this.plotToScrX = this.axisX.plotToScrLog.bind(this.axisX);
        this.plotToScrY = this.axisY.plotToScrLog.bind(this.axisY);
        this.scrToPlotX = this.axisX.scrToPlotLog.bind(this.axisX);
        this.scrToPlotY = this.axisY.scrToPlotLog.bind(this.axisY);
    }

    changeTitleOptions(newOptions) {
        this.titleOptions = newOptions;
    }

    changeShowHelpers(newHelperState) {
        this.isShowHelpers = newHelperState;
    }

    changeOptionsProject(optionsProject) {
        this.optionsProject = optionsProject;
    }

    changeOffsetLeft(newOffsetLeft) {
        this.offsetLeft = newOffsetLeft;
    }

    changeOptionsX(options) {
        const { min, max, viewScale } = options;
        this.axisX.min = min;
        this.axisX.max = max;
        this.axisX.viewScale = viewScale;
    }

    changeOptionsY(options) {
        const { min, max, viewScale } = options;
        this.axisY.min = min;
        this.axisY.max = max;
        this.axisY.viewScale = viewScale;
    }

    changeFullGrid(value) {
        this.fullGrid = value;
    }

    changeViewbox(newSize) {
        const { width, height } = newSize;

        if (this.fullGrid) {
            this.viewbox = {
                left: this.offsetLeft,
                top: 10,
                right: width - 20,
                bottom: height - 40
            };
            this.axisX.viewbox = { ...this.viewbox };
            this.axisY.viewbox = { ...this.viewbox };
        } else {
            this.heightLegend = this.getHeightLegend(this.legend.length);
            this.viewLegend = {
                left: 10,
                top: height - this.heightLegend,
                right: width - 20,
                bottom: height
            };
            this.viewbox = {
                left: this.offsetLeft,
                top: 40,
                right: width - 20,
                bottom: this.viewLegend.top
            };
            this.axisX.viewbox = { ...this.viewbox };
            this.axisY.viewbox = { ...this.viewbox };
        }
    }

    plotToScr(point) {
        return {
            x: this.plotToScrX(point.x),
            y: this.plotToScrY(point.y)
        }
    }

    scrToPlot(point) {
        return {
            x: this.scrToPlotX(point.x),
            y: this.scrToPlotY(point.y)
        }
    }

    build() {
        this.viewbox.bottom = this.viewLegend.top;
        this.axisX.build();
        this.axisY.build();
    }

    createCurve(options) {
        const { points, color, shadow } = options;
        
        let p = this.plotToScr(points[0]);
        let d = `M${p.x} ${p.y}`;
        for (let n = 1; n < points.length; n++) {
            p = this.plotToScr(points[n]);
            d += ` L${p.x} ${p.y}`;
        }
        if (options.region) d += ' Z';
        const curve = document.createElementNS(svgns, 'path');
        curve.setAttributeNS(null, 'd', d);
        curve.setAttributeNS(null, 'stroke-width', 2);
        curve.setAttributeNS(null, 'stroke', color);
        curve.setAttributeNS(null, 'fill', options.region ? getRGBA(color, 0.2) : 'none');
        curve.setAttributeNS(null, 'stroke-dasharray', shadow ? 4 : 0);
        return curve;
    }

    createWrapperCurves() {
        const g = document.createElementNS(svgns, 'g');
        g.classList.add('curves');
        return g;
    }

    addCurve(options) {
        this.curves.push(options);
    }

    clearCurves() {
        this.curves = [];
    }

    setMarkers(wrapperGroup, points = [], options) {
        const { marker, color } = options;
        const points_ = points.map(p => this.plotToScr(p));
        const pointsOfMarkers = getPointsOfMarkers(points_);
        pointsOfMarkers.forEach(point => {
            wrapperGroup.append(
                    drawMarker({
                        typeMarker: marker,
                        basePoint: point,
                        color
                    })
                );
        });
    }

    getHeightLegend(countGraph) {
        const sizeFont = 30;
        if (this.optionsProject.legend.height === 0 && this.optionsProject.legend.columns === 0) {
            if (countGraph === 1) return PADDING_LEGEND * 2 + sizeFont;
            if (countGraph > 1 && countGraph <= 4) return PADDING_LEGEND * 2 + sizeFont * 2 + MARGIN_CAPTION_LEGEND; 
            if (countGraph > 4 && countGraph <= 6) return PADDING_LEGEND * 2 + sizeFont * 3 + MARGIN_CAPTION_LEGEND * 2;
            if (countGraph > 6 && countGraph <= 12) return PADDING_LEGEND * 2 + sizeFont * 4 + MARGIN_CAPTION_LEGEND * 3;
            return sizeFont * 2;
        }

        if (this.optionsProject.legend.height === 0 && this.optionsProject.legend.columns > 0) {
            let curveIntoColumn;
            if (countGraph % this.optionsProject.legend.columns === 0) {
                curveIntoColumn = Math.round(countGraph / this.optionsProject.legend.columns);
            } else {
                curveIntoColumn = Math.trunc(countGraph / this.optionsProject.legend.columns) + 1;
            }
            return curveIntoColumn * sizeFont + curveIntoColumn * MARGIN_CAPTION_LEGEND;
        }  
    }

    drawLineSignatureX(canvas, signature) {
        const { x, text, color } = signature;
        const xScr = this.plotToScrX(x);
        const line = createLine({
            x1: xScr,
            y1: this.viewbox.top,
            x2: xScr,
            y2: this.viewbox.bottom + 25,
            options: {
                'stroke-dasharray': 4,
                'stroke-width': 2,
                stroke: color
            }
        });
        const textSVG = createText({ x: xScr, y: this.viewbox.bottom + 35, text, options: { 'text-anchor': 'middle', fill: color }});
        canvas.append(line, textSVG);
    }

    drawLineSignatureY(canvas, signature) {
        const { y, text, color } = signature;
        const yScr = this.plotToScrY(y);
        const line = createLine({
            x1: this.viewbox.left - 10,
            y1: yScr,
            x2:  this.viewbox.right,
            y2: yScr,
            options: {
                'stroke-dasharray': 4,
                'stroke-width': 2,
                stroke: color
            }
        });
        const textSVG = createText({ x: this.viewbox.left - this.offsetLeft + 10, y: yScr, text, options: {
            'dominant-baseline': 'middle',
            'text-anchor': 'start',
            fill: color
        }});
        canvas.append(line, textSVG);
    }

    drawCurves(canvas) {     
        let color = ''; 
        let title = '';
        if (this.curves.length !== 0) this.legend.legendCaptions = [];
        this.curves.forEach(curveEntity => {
            const wrapperCurves = this.createWrapperCurves();
            curveEntity.forEach(curveItem => {
                color = curveItem.color;
                title = curveItem.title;
                const curve = this.createCurve(curveItem);
                if (curveItem.region) {
                    this.setMarkers(wrapperCurves, curveItem.markers.points1, curveItem);
                    this.setMarkers(wrapperCurves, curveItem.markers.points2, curveItem);
                } else if (!curveItem.shadow) {
                    this.setMarkers(wrapperCurves, curveItem.points, curveItem);
                }
                wrapperCurves.append(curve);
                this.legend.legendCaptions.push({
                    title,
                    color
                });
            });
            canvas.append(wrapperCurves);
        });
    }

    drawNameAxisX(canvas) {
        const nameAxis = createText({
            x: this.viewbox.right,
            y: this.viewbox.bottom + 30 + +this.optionsProject.nameAxis.offsetTopAxisX,
            text: this.optionsProject.nameAxis.axisX,
            options: {
                'text-anchor': 'end',
                'dominant-baseline': 'hanging'
            },
            id: 'nameAxisX'
        });
        if (this.isShowHelpers.axis) {
            const width = this.viewbox.left + (this.viewbox.right - this.viewbox.left) * 0.5;
            const nameAxisRect = createRect({
                x: width,
                y: this.viewbox.bottom + 30,
                width,
                height: +this.optionsProject.nameAxis.offsetTopAxisX,
                fill: 'rgba(0, 255, 0, 0.3)',
                id: "nameAxisXRect"
            });
            canvas.append(nameAxisRect);
        }
        canvas.append(nameAxis);
    }

    drawNameAxisY(canvas) {
        const nameAxis = createText({
            x: this.viewbox.left - 20 + +this.optionsProject.nameAxis.offsetLeftAxisY,
            y: this.viewbox.top - 20,
            text: this.optionsProject.nameAxis.axisY,
            options: { 'text-anchor': 'start' },
            id: 'nameAxisY'
        });
        if (this.isShowHelpers.axis) {
            const nameAxisRect = createRect({
                x: this.viewbox.left - 20,
                y: this.viewbox.top - 20,
                width: this.optionsProject.nameAxis.offsetLeftAxisY,
                height: this.viewbox.top - 20,
                fill: 'rgba(0, 255, 0, 0.3)',
                id: "nameAxisYRect"
            });
            canvas.append(nameAxisRect);
        }
        canvas.append(nameAxis);
    }

    drawTitle(canvas) {
        if (this.isShowHelpers.title) {
            const leftOffsetRegion = createRect({
                x: this.viewbox.left,
                y: 0,
                width: this.optionsProject.title.offset,
                height: this.viewbox.top,
                fill: 'rgba(0, 255, 0, 0.3)',
                id: "leftOffset"
            });
            const rightOffsetRegion = createRect({
                x: this.viewbox.right - this.optionsProject.title.offset,
                y: 0,
                width: this.optionsProject.title.offset,
                height: this.viewbox.top,
                fill: 'rgba(0, 255, 0, 0.3)',
                id: "rightOffset"
            });
            canvas.append(leftOffsetRegion, rightOffsetRegion);
        }
        const txt = createText({
            x: 0,
            y: 0,
            text: this.optionsProject.title.projectName,
            id: 'titleProject',
            className: 'title-project'
        });
        canvas.append(txt);
        alignText({
            txt,
            align: this.optionsProject.title.align,
            viewbox: this.viewbox,
            offset: this.optionsProject.title.offset
        });
    }

    drawLegend(canvas) {
        let color;
        let curveIntoColumn;
        let positionCurve = placeDrawLegend;
        if (this.optionsProject.legend.columns > 0) {
            positionCurve = [];
            curveIntoColumn = Math.trunc(this.legend.length / this.optionsProject.legend.columns);
            if (this.legend.length % this.optionsProject.legend.columns > 0) curveIntoColumn = curveIntoColumn + 1;
            for (let i = 0; i < curveIntoColumn; i++) {
                for (let j = 0; j < this.optionsProject.legend.columns; j++) {
                    positionCurve.push({ x: i, y: j });
                }
            }
        } else {
            let y = 0;
            for (let n = 0; n < this.legend.length; n++) {
                if (placeDrawLegend[n].y > y) y = placeDrawLegend[n].y;
            }
            curveIntoColumn = y + 1;
        }

        const widthCurveCaption = (this.viewLegend.right - this.viewLegend.left) * curveIntoColumn / this.legend.length;
        const heightCurveCaption = 20;
        const g = createWrapper({ id: 'legend1' });
        this.legend.forEach((curve, n) => {
            if (color && color === curve.color) return;
            color = curve.color;
            const { x, y } = positionCurve[n];
            const line = createLine({
                x1: this.viewLegend.left + x * widthCurveCaption,
                y1: this.viewLegend.top + y * heightCurveCaption + 70,
                x2: this.viewLegend.left + x * widthCurveCaption + 90,
                y2: this.viewLegend.top + y * heightCurveCaption + 70,
                options: {
                    stroke: curve.color,
                    'stroke-width': 3
                }
            });
            g.append(line);
            for (let n = 1; n < 4; n++) {
                g.append(
                    drawMarker({
                        typeMarker: 'circle',
                        basePoint: { x: x + n * 22.5 + 10, y: y + this.viewLegend.top + y * heightCurveCaption + 70 },
                        color
                    })
                )
            }
            const text = createText({ x: x + 100, y: this.viewLegend.top + y * heightCurveCaption + 70, text: curve.title, options: { 'dominant-baseline': 'middle' }});
            text.style.fontSize = '16px';
            g.append(text);
        });
        canvas.append(g);
    }

    draw(canvas, signatureX, signatureY) {
        this.axisX.draw(canvas);
        this.axisY.draw(canvas);
        this.drawCurves(canvas);
        if (!this.fullGrid) {
            this.drawTitle(canvas);
            this.drawLegend(canvas);
            this.drawNameAxisX(canvas);
            this.drawNameAxisY(canvas);
        }
        if (signatureX.length > 0) {
            signatureX.forEach(item => this.drawLineSignatureX(canvas, item));
        }
        if (signatureY.length > 0) {
            signatureY.forEach(item => this.drawLineSignatureY(canvas, item));
        }
    }
}

export default Plot;