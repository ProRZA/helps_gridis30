import { aRound } from "../helpers/mathFunc";
import { createLine, createText } from '../helpers/elements';
import { config } from '../config';

class Axis {
    constructor() {
        this.min = 0;
        this.max = 0;
        this.viewbox = {};
        this.viewScale = 'log';
        this.axis = [];
        this.scale = 1;
        this.step = 0;
    }

    createText(axis, x, y, text) {
        return createText({ x, y, text, options: { 'dominant-baseline': axis === 'y' ? 'middle' : "", 'text-anchor': 'middle' }});
    }

    createLine(data) {
        const { color, width, x1, y1, x2, y2 } = data;
        return createLine({ x1, y1, x2, y2, options: { stroke: color, 'stroke-width': width } });
    }

    getBasis(v) {
        const av = Math.floor(v);
        if (v - av === 0) return v;
        return av;
    }

    findStepLinear() {
        const STEP_RATION = [2, 5, 10];

        const range = this.max - this.min;
        const basis = this.getBasis(Math.log10(range));

        const basisStep = STEP_RATION.find(step => (Math.floor(range / aRound(step*Math.pow(10, basis - 1)))) <= 10);
        return basisStep*Math.pow(10, basis-1);
    }

    findStepLog() {
        return Math.log10(this.max / this.min);
    }

    findStep() {
        return this.viewScale === 'log' ? this.findStepLog() : this.findStepLinear();
    }

    findScale(sizeViewbox) {
        return this.viewScale === 'log' ? 
        sizeViewbox / Math.log10(this.max / this.min) : 
        sizeViewbox / (this.max - this.min);
    }
}

class AxisX extends Axis {
    constructor(offset) {
        super();
        this.offset = offset;
    }

    createMainAxis(index) {
        return this.viewScale === 'log' ? {
            p: this.scale * index + this.viewbox.left,
            txt: Math.pow(10, index + Math.log10(this.min))
        } : {
            p: this.step * index * this.scale + this.viewbox.left,
            txt: this.step * index
        }
    }

    build() {
        this.axis = [];
        this.step = this.findStep();
        this.scale = this.findScale(this.viewbox.right - this.viewbox.left);
        const count = this.viewScale === 'log' ? this.step : (this.max - this.min) / this.step;
        for (let n = 0; n <= count; n++) {
            this.axis.push(this.createMainAxis(n))
        }
    }

    plotToScrLinear(value) {
        return Math.round(config.OFFSET_FROM_LEFT + (value - this.min) * this.scale);
    }

    scrToPlotLinear(value) {
        return (value - config.OFFSET_FROM_LEFT) / this.scale + this.min;
    }

    plotToScrLog(value) {
        return Math.round(this.offset + (Math.log10(value) - Math.log10(this.min)) * this.scale); 
    }

    scrToPlotLog(value) {
        return Math.pow(10, (value - config.OFFSET_FROM_LEFT) / this.scale + Math.log10(this.min));
    }

    drawMain(canvas) {
        const { colorMainLine: color, widthMainLine: width } = config;
        this.axis.forEach(x => {
            const line = this.createLine({
                color,
                width,
                x1: x.p,
                y1: this.viewbox.top,
                x2: x.p,
                y2: this.viewbox.bottom
            });
            const txt = this.createText('x', x.p, this.viewbox.bottom + 20, x.txt);
            canvas.append(line, txt);
        });
    }

    drawMinorLinear(canvas) {
        const { colorMinorLine: color, widthMinorLine: width } = config;
        const axisMinor = this.axis.slice(0, -1);
        const stepMinor = this.step / 10 * this.scale;
        axisMinor.forEach(x => {
            for (let n = 1; n < 10; n++) {
                const dx = x.p + stepMinor * n;
                const line = this.createLine({
                    color,
                    width,
                    x1: dx,
                    y1: this.viewbox.top,
                    x2: dx,
                    y2: this.viewbox.bottom
                });
                canvas.append(line);
            }
        });
    }

    drawMinorLog(canvas) {
        const { colorMinorLine: color, widthMinorLine: width } = config;
        const axisMinor = this.axis.slice(0, -1);
        axisMinor.forEach(x => {
            for (let n = 1; n < 10; n++) {
                const dx = x.p + config.logRange[n-1] * this.scale;
                const line = this.createLine({
                    color,
                    width,
                    x1: dx,
                    y1: this.viewbox.top,
                    x2: dx,
                    y2: this.viewbox.bottom
                });
                canvas.append(line);
            }
        });
    }

    draw(canvas) {
        this.drawMain(canvas);
        this.viewScale === 'log' ? this.drawMinorLog(canvas) : this.drawMinorLinear(canvas);
    }
}

class AxisY extends Axis {
    constructor() {
        super();
    }

    createMainAxis(index) {
        return this.viewScale === 'log' ? {
            p: this.viewbox.bottom - this.scale * index,
            txt: Math.pow(10, index + Math.log10(this.min))
        } : {
            p: this.viewbox.bottom - this.step * index * this.scale,
            txt: this.step * index
        }
    }

    build() {
        this.axis = [];
        this.step = this.findStep();
        this.scale = this.findScale(this.viewbox.bottom - this.viewbox.top);
        const count = this.viewScale === 'log' ? this.step : (this.max - this.min) / this.step;
        for (let n = 0; n <= count; n++) {
            this.axis.push(this.createMainAxis(n));
        }
    }

    plotToScrLinear(value) {
        return Math.round((this.viewbox.bottom + 0) - (value - this.min) * this.scale);
    }

    scrToPlotLinear(value) {
        return ((config.OFFSET_FROM_BOTTOM) - value) / this.scale + this.min; 
    }

    plotToScrLog(value) {
        return Math.round(this.viewbox.bottom - (Math.log10(value) - Math.log10(this.min)) * this.scale); 
    }

    scrToPlotLog(value) {
        return Math.pow(10, (config.OFFSET_FROM_BOTTOM - value) / this.scale + Math.log10(this.min));
    }

    drawMain(canvas) {
        const { colorMainLine: color, widthMainLine: width } = config;

        this.axis.forEach(y => {
            const line = this.createLine({
                color,
                width,
                x1: this.viewbox.left,
                y1: y.p,
                x2: this.viewbox.right,
                y2: y.p
            });
            const txt = this.createText('y', this.viewbox.left - 20, y.p, y.txt);
            canvas.append(line, txt);
        });
    }

    drawMinorLinear(canvas) {
        const { colorMinorLine: color, widthMinorLine: width } = config;
        const axisMinor = this.axis.slice(0, -1);
        const stepMinor = this.step / 10 * this.scale;
        axisMinor.forEach(y => {
            for (let n = 1; n < 10; n++) {
                const dy = y.p - stepMinor * n;
                const line = this.createLine({
                    color,
                    width,
                    x1: this.viewbox.left,
                    y1: dy,
                    x2: this.viewbox.right,
                    y2: dy
                });
                canvas.append(line);
            }
        });
    }

    drawMinorLog(canvas) {
        const { colorMinorLine: color, widthMinorLine: width } = config;
        const axisMinor = this.axis.slice(0, -1);
        const stepMinor = this.step / 10 * this.scale;
        axisMinor.forEach(y => {
            for (let n = 1; n < 10; n++) {
                const dy = y.p - config.logRange[n-1] * this.scale;
                const line = this.createLine({
                    color,
                    width,
                    x1: this.viewbox.left,
                    y1: dy,
                    x2: this.viewbox.right,
                    y2: dy
                });
                canvas.append(line);
            }
        });
    }

    draw(canvas) {
        this.drawMain(canvas);
        this.viewScale === 'log' ? this.drawMinorLog(canvas) : this.drawMinorLinear(canvas);
    }
}

export { AxisX, AxisY };