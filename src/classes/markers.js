import { config } from '../config';
import { hypotenuse, atg } from '../helpers/mathFunc';

const circle = (basePoint, color, radius = 5) => {
    const circle = document.createElementNS(config.svgns, 'ellipse');
    circle.setAttributeNS(null, 'cx', basePoint.x);
    circle.setAttributeNS(null, 'cy', basePoint.y);
    circle.setAttributeNS(null, 'rx', radius);
    circle.setAttributeNS(null, 'ry', radius);
    circle.setAttributeNS(null, 'fill', color);
    return circle;
} 

const drawMarker = options => {
    const { typeMarker, basePoint, color } = options;
    switch (typeMarker) {
        case 'circle': return circle(basePoint, color);
        default: return circle(basePoint, color);
    }
}

const getPointsOfMarkers = (points, step = 100) => {
    const pts = [...points];
    let delta = 0;
    let n = 0;
    const result = [];

    while (n < pts.length - 1) {
        const L = hypotenuse(pts[n], pts[n + 1]);
        if (L + delta < step) {
            delta = delta + L;
            n++;
        } else {
            let balance = step - delta;
            let a = atg(pts[n], pts[n + 1]);
            if (a < 0) {// если угол отрицательный, то пропускаем и идем дальше
                n++;
                continue;
            }
            let p = {
                x: Math.round(pts[n].x + balance * Math.cos(a)),
                y: Math.round(pts[n].y + balance * Math.sin(a))
            }
            result.push(p);
            delta = 0;
            pts[n] = p;
        }
    }
    return result;
}

export { getPointsOfMarkers, drawMarker };