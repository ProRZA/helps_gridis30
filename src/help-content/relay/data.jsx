import React, { useState } from 'react';

import Section from '../../components/panels/relay/section';
import PlotGraph from '../../components/plot-graph';
import Viewport from '../../components/viewport';

import { config } from '../../config';

const DataRelay = ({

}) => {
    const [offIots, setOffIost] = useState(false);

    return (
        <div className="help">
            <div className="title">Данные панели "Реле"</div>
            <div className="content">
            <p>Изменение уставок происходит при вводе с клавиатуры после подтверждения кнопкой "Enter", либо при переходе на другое поле или другой элемент.
                 Каждое реле имеет свои диапазоны допустимых значений как по току, так и по времени. 
                 Значении, которое выходит за границы текущего диапазона, будет автоматически приведено к ближайшиму допустимому значению уставки.</p>
                <Viewport
                    modifier="base-panel relay"
                    signature="Хар-ка реле РТ-81/1, PT-83/1, PT-85/1."
                >
                    <Section
                        settings={{
                            inom: {
                                labelText: "Icp., A",
                                value: '350' 
                            },
                            tc: {
                                labelText: 'tco., c',
                                value: '4'
                            },
                            iots: {
                                labelText: 'Iотс., о.е.',
                                value: '3'
                            }
                        }}
                        off={offIots}
                        changeOff={v => setOffIost(v)}
                    />
                </Viewport>
                <p className="attention">Для просмотра как будет изменяться вид кривой, нажмите чекбокс OFF.</p>
                <Viewport
                    isStretsh
                    signature="Хар-ка реле РТ-81/1, PT-83/1, PT-85/1."
                >
                    <PlotGraph
                        showCurve={offIots ? ["Relay_off_iots"] : ["Relay_two_settings"]}
                        minX={100}
                        maxX={10000}
                        minY={0.01}
                        maxY={10000}
                        offsetLeft={50}
                        signatureX={offIots ? [
                            { x: 350, text: 'Iср', color: config.redBorder }
                        ] : [
                            { x: 10, text: 'Iср', color: config.redBorder },
                            { x: 30, text: 'Iотс = 30A', color: config.redBorder }
                        ]}
                        signatureY={[
                            { y: 4, text: 'tсо.,с', color: config.redBorder }
                        ]}
                    />
                </Viewport>
            </div>
        </div> 
    );
}

export default DataRelay;