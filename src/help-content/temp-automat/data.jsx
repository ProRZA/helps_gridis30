import React from 'react';

import Section from '../../components/panels/module/section';
import PlotGraph from '../../components/plot-graph';
import Viewport from '../../components/viewport';

import { config } from '../../config';

const DataTempAutomat = ({

}) => {
    return (
        <div className="help">
            <div className="title">Данные панели."</div>
            <div className="content">
                <p>Характеристика термомагнитного автомата показана областью с учетом разброса.</p>
                <p>Для каждой характеристики имеется два режима: холодное состояние и горячее состония (под нагрузкой).</p>
                <Viewport
                    modifier="base-panel module"
                    signature="Автомат Optimat 100. Нагретое состояние, 16А"
                >
                    <Section
                        inValue="16"
                        curveValue="Нагретое состояние"
                    />
                </Viewport>
                <Viewport
                    signature="Автомат Optimat 100. Нагретое состояние, 16А"
                    isStretsh
                >
                    <PlotGraph
                        modifier="flex hc mt-15"
                        modifierSVG="shadow"
                        showCurve={["Temp_Automat_Hot"]}
                        minX={10}
                        maxX={10000}
                        minY={0.01}
                        maxY={10000}
                        signatureX={[
                            { x: 16, text: 'In', color: config.redBorder }
                        ]}
                    />
                </Viewport>
            </div>
        </div> 
    );
}

export default DataTempAutomat;