import React from 'react';

import { Section, DependSection } from '../../components/panels/characteristic/section.jsx';
import PlotGraph from '../../components/plot-graph/index.jsx';
import Wrapper from '../../components/wrapper.jsx';
import Viewport from '../../components/viewport.jsx';

import { config } from '../../config';
import { curves } from '../../components/curves/basic-curve';

const DataStandart = ({

}) => {
    const characters = [
        'INV', 
        'VERY',
        'EXT',
        'L',
        'RI',
        'RTB1',
        'RTB80',
        'I2t',
        'I4t'
    ]
    const { redBorder, greenBorder, blueBorder } = config;
    return (
        <div className="help">
            <div className="title">Данные панели "стандартная характеристика"</div>
            <div className="content">
                <div className="mini_caption gradient_caption">Построение характеристики с независимой третьей ступенью</div>
                <Wrapper>
                    <p>При построении характеристики с независимой третьей ступенью кривая задается в виде прямолинейных участков функции. 
                        Функция задается в виде пары значений "Ток-Время".</p>

                    <ul>
                        <li>Данные заполняются без пропусков.</li>
                        <li>Уставки задаются по возрастанию тока.</li>
                    </ul>
                    <Viewport
                        modifier="base-panel mb-10"
                        signature="Вид данных с независимой третью ступенью"
                    >
                        <Section
                            modifier="mlr-10 light-border red"
                            sectionName="Третья ступень"
                            labelLeftInput="I>, A"
                            labelRightInput="t>, c"
                            leftInputValue="10"
                            rightInputValue="1000"
                        />
                        <Section
                            modifier="mlr-10 light-border green"
                            sectionName="Вторая ступень"
                            labelLeftInput="I>>, A"
                            labelRightInput="t>>, c"
                            leftInputValue="50"
                            rightInputValue="5"
                        />
                        <Section
                            modifier="mlr-10 light-border blue"
                            sectionName="Первая ступень"
                            labelLeftInput="I>>>, A"
                            labelRightInput="t>>>, c"
                            leftInputValue="120"
                            rightInputValue="0.1"
                        />
                    </Viewport>
                    <Viewport
                        signature="Кривая с независимой третью ступенью"
                        isStretsh
                    >
                        <PlotGraph
                            modifier="flex hc"
                            modifierSVG="shadow"
                            showCurve={["independedStandart"]}
                            maxX={1000}
                            offsetLeft={70}
                            signatureX={[
                                { x: 10, text: 'I>', color: redBorder },
                                { x: 50, text: 'I>>', color: greenBorder},
                                { x: 120, text: 'I>>>', color: blueBorder }
                            ]}
                            signatureY={[
                                { y: 1000, text: 't>', color: redBorder },
                                { y: 5, text: 't>>', color: greenBorder },
                                { y: 0.1, text: 't>>>', color: blueBorder },
                            ]}
                        /> 
                    </Viewport>
                </Wrapper>

                <div className="mini_caption gradient_caption">Построение характеристики с зависимой третьей ступенью</div>
                <Wrapper>
                    <p>При построении характеристики с зависимой третьей ступенью кривая задается в виде времятоковой характеристики в области «Токовая перегрузка»</p>
                    <p>Эта ступень представляет собой обратно-инверсную зависимость времени от тока t=f(i).</p>
                    <p>В областях первой и второй ступени характеристика строится прямолинейными кусочными графиками (аналогично случаю с независимой третьей ступенью)</p>
                    <p>Для того, чтобы была возможность задать график t=f(i) секция "Третья ступень" меняет свой вид.</p>
                    <Viewport
                        signature={`Вид секции "Третья ступень" с зависимой третью ступенью`}
                        modifier="base-panel mb-10"
                    >
                        <DependSection/>
                    </Viewport>
                    <p>Для построения защитной кривой необходимо указать вид функции, который выбирается через список "Тип характеристики".</p>
                    <p>В Гридис-КС существует 9 функций. Пример их вида можно посмотреть ниже.</p> 
                    <div className="flex spc-ard wrap pb-10">
                        {characters.map((item, index) => (
                            <Viewport
                                key={index}
                                signature={`${index + 1}) ${curves[item].title}`}
                            >
                                <PlotGraph
                                    width={300}
                                    height={200}
                                    modifier="flex hc"
                                    modifierSVG="shadow"
                                    showCurve={[item]}
                                    minX={1}
                                    minY={10}
                                    maxX={100}
                                    signatureX={[
                                        { x: 10, text: 'Iпуск', color: redBorder },
                                        { x: 30, text: '30A', color: blueBorder},
                                        { x: 50, text: 'I>>', color: redBorder }
                                    ]}
                                    signatureY={[
                                        { y: 50, text: '50c', color: blueBorder },
                                    ]}
                                    fullGrid
                                /> 
                            </Viewport>
                        ))}
                    </div>
                    
                    <p>Построение функция защитной кривой начинается на токе "Iпуск" и ограничивается следующей степенью защиты.</p>
                    <p>Поля "Iсогл., А" и "tсогл., с" задают точку, через которую пройдет защитная кривая.</p>
                    <p>Таким образом:</p>
                    <ul>
                        <li>«Iпуск» - пусковой ток. Начальный ток для построения характеристики.</li>
                        <li>«Iсогл» - ток согласования защиты. Ток через который проходит характеристика.</li>
                        <li>«tсогл» - время согласования защиты. Время через которое проходит характерстика.</li>
                    </ul>

                    <Viewport
                        signature="Вид данных с зависимой третью ступенью"
                        modifier="base-panel mb-10"
                    >
                         <DependSection
                            modifier="light-border red"
                        />
                        <Section
                            modifier="mlr-10 light-border green"
                            sectionName="Вторая ступень"
                            labelLeftInput="I>>, A"
                            labelRightInput="t>>, c"
                            leftInputValue="50"
                            rightInputValue="5"
                        />
                        <Section
                            modifier="mlr-10 light-border blue"
                            sectionName="Первая ступень"
                            labelLeftInput="I>>>, A"
                            labelRightInput="t>>>, c"
                            leftInputValue="120"
                            rightInputValue="0.1"
                        />
                    </Viewport>
                

                    <Viewport
                        signature="Кривая с зависимой третью ступенью"
                        isStretsh
                    >
                        <PlotGraph
                            modifier="flex hc"
                            modifierSVG="shadow"
                            showCurve={["dependedStandart"]}
                            maxX={1000}
                            signatureX={[
                                { x: 10, text: 'Iпуск', color: redBorder },
                                { x: 30, text: 'Iсогл', color: redBorder },
                                { x: 50, text: 'I>>', color: greenBorder},
                                { x: 120, text: 'I>>>', color: blueBorder }
                            ]}
                            signatureY={[
                                { y: 50, text: 'tсогл', color: redBorder},
                                { y: 5, text: '5c', color: greenBorder },
                                { y: 0.1, text: '', color: blueBorder },
                            ]}
                        /> 
                    </Viewport>
                    <p className="mt-15">Коэффициент k получается расчетным путем. Данный коэффициент характеризует наклон и высоту защитной кривой.</p>
                    <p>Если в исходных данных уже имеется коэффициент k, то его можно указать включив чекбокс "Через коэф. K".
                        При это поля "Iсогл., А" и "tсогл., с" будут скрыты, а поле "Коэф. K" станет доступным для внесения данных.
                    </p>
                </Wrapper>
            </div>
        </div>
    )
};

export default DataStandart;