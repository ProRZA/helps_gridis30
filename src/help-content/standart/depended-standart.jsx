import React, { useState } from 'react';

import Radio from '../../components/Radio';
import PlotGraph from '../../components/plot-graph';
import Viewport from '../../components/viewport';

const DependedStandart = () => {
    const [curve, setCurve] = useState(false);

    return (
        <div className="help">
            <div className="title">Вид крайней ступени</div>
            <div className="content">
                <p>Переключатель "Вид крайней ступени" позволяет задать правила построения для третьей ступени стандартной характеристики.</p>
                <Viewport
                    modifier="w285px"
                    signature="Переключите вид краней ступени"
                >
                    <Radio
                        modifier="mlr-5 mt-15 mb-15"
                        caption="Вид крайней ступени"
                        activeValue={curve}
                        radioList={[
                            {
                                text: 'Независимая',
                                value: false
                            },
                            {
                                text: 'Зависимая',
                                value: true
                            }
                        ]}
                        setValue={v => setCurve(v)}
                    />
                </Viewport>
            
                <Viewport
                    isStretsh
                    signature="Вид крайней ступени в зависимости от положения радио кнопки"
                >
                    <PlotGraph
                        modifier="flex hc mt-15"
                        modifierSVG="shadow"
                        showCurve={curve ? ["dependedStandart"] : ["independedStandart"]}
                        maxX={1000}
                    />
                </Viewport>
                
            </div>
        </div>
    );
};

export default DependedStandart;