import React, { useState, useEffect } from 'react';

import Wrapper from '../../components/wrapper';
import Radio from '../../components/Radio';
import PlotGraph from '../../components/plot-graph';
import Viewport from '../../components/viewport';

const StepCount = () => {
    const [steps, setSteps] = useState('threeSteps');
    const [curve, setCurve] = useState(['dependedStandart']);

    useEffect(() => {
        switch (steps) {
            case 'threeSteps':
                setCurve(["dependedStandart"]);
                break;
            case 'twoSteps':
                setCurve(["dependedStandart_two_steps"]);
                break;
            case 'oneStep':
                setCurve(["dependedStandart_one_steps"]);
                break;
        }
    }, [steps]);

    return (
        <div className="help">
            <div className="title">Количество ступеней</div>
            <div className="content">
                <Wrapper>
                    <p>Переключатель "Количество ступеней" позволяет задать количество ступеней защитной кривой стандартной характеристики.</p>
                    <Viewport
                        modifier="w285px mb-10"
                        signature="Измените количество ступеней"
                    >
                        <Radio
                            modifier="mlr-5 mt-20"
                            caption="Количество ступеней"
                            activeValue={steps}
                            radioList={[
                                {
                                    text: '1',
                                    value: 'oneStep'
                                },
                                {
                                    text: '2',
                                    value: 'twoSteps'
                                },
                                {
                                    text: '3',
                                    value: 'threeSteps'
                                }
                            ]}
                            setValue={v => setSteps(v)}
                        />
                    </Viewport>

                    <Viewport
                        isStretsh
                        signature="Вид характеристики в зависимости от количества ступеней"
                    >
                        <PlotGraph
                            modifier="flex hc mt-15"
                            modifierSVG="shadow"
                            showCurve={curve}
                            maxX={1000}
                        />
                    </Viewport>
                    
                </Wrapper>
                <Wrapper>
                    <p className="mt-20">Стандартные кривые содержат до трех ступеней и как правило, каждая ступень характеризует свою защиту:</p>
                    <div className="ml-20">
                        <p className="mt-10"><span className="fb">-Третья ступень</span> - "Защита от перегрузки".</p>
                        <p>Может быть как зависимой, так и независимой. Настраивается через переключатели "Вид крайней ступени"</p>
                        <Viewport
                            modifier="w285px"
                            signature="Переключатель вида крайней ступени"
                        >
                            <Radio
                                modifier="mlr-5 mt-15 mb-15"
                                caption="Вид крайней ступени"
                                activeValue={false}
                                radioList={[
                                    {
                                        text: 'Независимая',
                                        value: false
                                    },
                                    {
                                        text: 'Зависимая',
                                        value: true
                                    }
                                ]}
                            />
                        </Viewport>
                        <p className="mt-10"><span className="fb">-Вторая ступень</span> - "Максимальная токовая защита"</p>
                        <p className="mt-10"><span className="fb">-Первая ступень</span> - "Мгновенная токовая отсечка"</p>
                    </div>
                    <p className="mt-10">Ступени идут от третьей к первой. Таким образом при двух ступенях защиты, первая ступень станет недоступной.</p>
                </Wrapper>
            </div>
        </div>
    );
};

export default StepCount;