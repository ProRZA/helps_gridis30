import React from 'react';

import Section from '../../components/panels/module/section';
import PlotGraph from '../../components/plot-graph';
import Viewport from '../../components/viewport';

import { config } from '../../config';

const DataModuleAutomat = ({

}) => {
    return (
        <div className="help">
            <div className="title">Данные панели "Модульные автоматы"</div>
            <div className="content">
                <p>Характеристика модульного автомата показана областью с учетом теплового разброса.</p>
                <p>Каждой характеристика имеется две уставки: номинальный ток и кривая, показывающая кратность допустимого тока.</p>
                <Viewport
                    signature='Модульный автомат Multi(C120H). Кривая "C", 2А'
                    modifier="base-panel module"
                >
                    <Section
                        inValue="2"
                        curveValue="C"
                    />
                </Viewport>
                <Viewport
                    isStretsh
                    signature='Модульный автомат Multi(C120H). Кривая "C", 2А'
                >
                    <PlotGraph
                        showCurve={["Module_Automat"]}
                        minX={1}
                        maxX={100}
                        minY={0.01}
                        maxY={10000}
                        signatureX={[
                            { x: 2, text: 'In', color: config.redBorder }
                        ]}
                    />
                </Viewport>
            </div>
        </div> 
    );
}

export default DataModuleAutomat;