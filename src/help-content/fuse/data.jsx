import React from 'react';

import PlotGraph from '../../components/plot-graph';
import Input from '../../components/input';
import Viewport from '../../components/viewport';

import { config } from '../../config';

const DataFuse = ({

}) => {
    return (
        <div className="help">
            <div className="title">Данные панели "Предохранители"</div>
            <div className="content">
                <p>Характеристика предохранителей может быть показаны областью с учетом теплового разброса и одной кривой без учета разброса.</p>
                <p>Каждая характеристика имеет только одну уставку: номинальный ток In, в амперах</p>
                <Viewport
                    modifier="base-panel fuse margin-none"
                    signature="Предохранитель"
                >
                    <Input
                        modifier="fuse mb-15"
                        inputModifier="ml-5"
                        labelPosition="left"
                        labelText="In, A"
                        value="31,5"
                        iconType="spin"
                    />
                </Viewport>
                <Viewport
                    isStretsh
                    signature="Хар-ка предохранииеля в одну линию без разброса."
                >
                    <PlotGraph
                        modifier="flex hc"
                        modifierSVG="shadow"
                        showCurve={["Fuse_line"]}
                        minX={10}
                        maxX={10000}
                        minY={0.01}
                        maxY={10000}
                    />
                </Viewport>
                
                <Viewport
                    modifier="base-panel fuse margin-none"
                    signature="Предохранитель"
                >
                    <Input
                        modifier="fuse mb-15"
                        inputModifier="ml-5"
                        labelPosition="left"
                        labelText="In, A"
                        value="25"
                        iconType="spin"
                    />
                </Viewport>
                <Viewport
                    isStretsh
                    signature="Областная хар-ка предохранииеля с учтеом разброса."
                >
                    <PlotGraph
                        modifier="flex hc"
                        modifierSVG="shadow"
                        showCurve={["Fuse_region"]}
                        minX={10}
                        maxX={10000}
                        minY={0.01}
                        maxY={10000}
                    />
                </Viewport>
            </div>
        </div> 
    );
}

export default DataFuse;