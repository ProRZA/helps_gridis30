import React from 'react';

import Wrapper from '../../components/wrapper';
import Input from '../../components/input';
import Icon from '../../components/icon';
import PlotGraph from '../../components/plot-graph';
import Viewport from '../../components/viewport';

import optionCurves from '../../img/option-curve.gif';

const OptionsPanel = () => (
    <div className="help">
        <div className="title">Настройка кривых</div>
        <div className="content">
            <p>В нижней части панели располагаются элементы управления, которые позволяют задать вид кривой на графческой области построения.</p>
            <Wrapper>
                <div className="mini_caption gradient_caption">Отсечение кривой по току</div>
                <p>Для того, чтобы отсечь по току часть характеристики укажите в поле</p>
                <div className="inline-block ml-5 mr-5">
                <Input
                    inputModifier="w85px"
                    labelText="Imax(A)&nbsp;=&nbsp;"
                    labelPosition="left"
                    iconType="save"
                    value="70"
                />
                </div>
                <span>значение в амперах и нажмите клавишу Enter, или кнопку</span>
                <Icon modifier="inline-block ml-5 mr-5" type="save"/>
                <Viewport
                    isStretsh
                    signature="На карте изображена кривая срезанная по значению 70А"
                >
                    <PlotGraph
                        modifier="flex hc"
                        modifierSVG="shadow"
                        showCurve={["basicLimit"]}
                        signatureX={[{ x: 70, text: '70A', color: '#ff0f00'} ]}
                        minX={10}
                        maxX={100}
                        minY={10}
                        maxY={10000}
                    /> 
                </Viewport>
            </Wrapper>

            <Wrapper>
                <div className="mini_caption gradient_caption">Стилизация кривых</div>
                <span>При нажатии на кнопку</span>
                <svg
                    id="identification"
                    className="inline-block crs-point ml-5 mr-5"
                    width="55"
                    height="40"
                >
                    <ellipse cx="27" cy="20" rx="5" ry="5"/>
                </svg>
                <span>откроется окно для стилизации кривой характеристики.</span>
                <p>В этом окне можно задать цвет, толщину кривой, и вид ее маркеров.</p>
                <p>При изменении параметров, в правой части окна будет выводится измененный вид кривой</p>
                <Viewport
                    modifier="w285px"
                    signature="Окно &laquo;Настройки кривых&raquo;"
                >
                    <img className="image" src={optionCurves} />
                </Viewport>
            </Wrapper>
        </div>
    </div>
);

export default OptionsPanel;