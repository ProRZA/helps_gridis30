import React from 'react';

import Grid from '../../components/grid';
import Icon from '../../components/icon';

const Buttons = ({ panel }) => (
    <div className="help">
        <div className="title">Общие элементы управления</div>
        <div className="content">
            <p>В нижней части панели находятся кнопки управления.</p>
            <Grid modifier="mt-20" columns={[
                    {
                        column: 2,
                        modifier: 'flex h-end vc mr-20',
                        content: (
                            <Icon modifier="ml-5 mr-5" type="simafor"/>
                        ),
                    },
                    {
                        column: 10,
                        content: (
                            <div className="">
                                <span>Данная кнопка позволяет перевести панель во включенное (активное), либо в отключенное (неактивное) состояние.</span>
                                <span>При отключенном состоянии панели, с графической области построения исчезает ее кривая. В данном режиме кривая не участвует в обработке данных.</span>
                            </div>
                        )
                    }
                ]}
            />
            {panel === 'Standart' && (
                <>
                    <Grid modifier="mt-20" columns={[
                        {
                            column: 2,
                            modifier: 'flex h-end vc mr-20',
                            content: (
                                <button style={{ height: '30px' }} className="center">Обновить</button>
                            ),
                        },
                        {
                            column: 10,
                            content: (
                                <div className="">
                                    <p>Кнопка позволяет создать и/или редактировать существующую характеристику</p>
                                </div>
                            )
                        }
                    ]}
                    />
                    <Grid modifier="mt-20" columns={[
                            {
                                column: 2,
                                modifier: 'flex h-end vc mr-20',
                                content: (
                                    <button style={{ height: '30px' }} className="center">Очистить</button>
                                ),
                            },
                            {
                                column: 10,
                                content: (
                                    <div className="">
                                        <p>Данная кнопка очищает панель "Стандартная характеристика" и возвращает ее в исходное состояние.</p>
                                    </div>
                                )
                            }
                        ]}
                    />
                </>
            )}
            <Grid modifier="mt-20" columns={[
                    {
                        column: 2,
                        modifier: 'flex h-end vc mr-20',
                        content: (
                            <Icon modifier="inline-block ml-5 mr-5" type="delete"/>
                        ),
                    },
                    {
                        column: 10,
                        content: (
                            <div className="">
                                <span>Данная кнопка удалит панель и ее характеристику на карте селективности.</span>
                            </div>
                        )
                    }
                ]}
            />
        </div>
    </div>
);

export default Buttons;