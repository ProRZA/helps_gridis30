import React from 'react';

import PlotGraph from '../../components/plot-graph';
import Icon from '../../components/icon';
import Viewport from '../../components/viewport';

import { config } from '../../config';

const NameCharacter = () => (
    <div className="help">
        <h2 className="title">Поле "Имя характеристики"</h2>
        <div className="content">
            <p>Данное имя будет отображаться в легенде карты селективности.</p>
            <div style={{ textIndent: '20px', fontSize: '14px'}}>Для принятия нового имени характеристики нажмите клавишу Enter, 
            или иконку <Icon modifier="inline-block ml-5 mr-5" type="save"/></div>
            <Viewport
                signature="Легенда отображается снизу графика"
                isStretsh
            >
                <PlotGraph
                    modifier="flex hc"
                    modifierSVG="shadow"
                    showCurve={["basicName"]}
                    minX={10}
                    maxX={100}
                    minY={10}
                    maxY={10000}
                /> 
            </Viewport>
        </div>
    </div>
);

export default NameCharacter;