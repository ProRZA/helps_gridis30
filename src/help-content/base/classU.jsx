import React from 'react';

import Wrapper from '../../components/wrapper';
import Radio from '../../components/Radio';
import Network from '../../components/panels/network';
import Viewport from '../../components/viewport';

const ClassU = () => (
    <div className="help">
        <div className="title">Выбор класса напряжения характеристики</div>
        <div className="content">
            <Wrapper>
                <Viewport
                    signature="Выбор базового класса напряжения для характеристики"
                    imageModifier="white"
                >
                    <Radio
                        modifier="mlr-5 mt-15 mb-10"
                        caption="Приведено к Uср, кВ"
                        activeValue={0.4}
                        radioList={[
                            {
                                modifier: 'direction',
                                text: '0.4',
                                value: 0.4
                            },
                            {
                                modifier: 'direction',
                                text: '6.3',
                                value: 6.3
                            },
                            {
                                modifier: 'direction',
                                text: '10.5',
                                value: 10.5
                            }
                        ]}
                    />
                </Viewport>
                <p>При выборе класса напряжения, данная характеристика будет пересчитана к базовому классу напряжения сети по формуле U<sub>хар.</sub>/U<sub>сети</sub>.</p>
            </Wrapper>
            <Wrapper>
                <p>Базовое напряжение сети задается в панели "Расчетное напряжение"</p>
                <Viewport
                    signature="Выбор базового класса напряжения для сети"
                    colorBakground="btn-face"
                >
                    <Network modifier="shadow" />
                </Viewport>
            </Wrapper>
        </div>
    </div>
);

export default ClassU;