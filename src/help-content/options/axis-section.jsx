import React, { useState } from 'react';

import OptionsAxis from '../../components/options-project/options-axis';
import PlotGraph from '../../components/plot-graph';
import Viewport from '../../components/viewport';
import { config } from '../../config';

const DataAxisSection = ({

}) => {
    const { optionsProject, isShowHelpers } = config;
    const [nameAxis, setNameAxis] = useState({
        axisX: 'Ток, А',
        axisY: 'Время, с',
        offsetTopAxisX: 10,
        offsetLeftAxisY: 10
    });

    const validOffset = v => {
        if (v < 0) return 0;
        if (v > 30) return 30;
        return v;
    }

    const nameAxisHandler = (axis, action) => value => {
        if (!['changeName', 'changeOffset'].includes(action)) return;
        let v = value;
        if (action === 'changeName' && v.length > 30) return;
        if (action === 'changeOffset') {
            v = v.replace(/\D/, "");
            v = validOffset(Number(v));
        }
        setNameAxis({ ...nameAxis, [axis]: v });
    }

    return (
        <div className="help">
            <div className="title">Данные окна настроек "Настройка подписей осей"</div>
            <div className="content">
                <p>Секция "Настройка подписей осей" отвечает за значение и позиционирование осей.</p>
                <p>По умолчанию ось Х имеет текст "Ток, А", а ось Y - "Время, с".</p>
                <p>Выравнивание заголовка позволяет прижать надпись по левому краю, по центру (по умолчанию) и по правому краю.</p>      
                <p>Отступы по бокам уменьшают область отрисовки текста. 
                    Если текст не помещается в область отрисовки, то он разбивается на несколько строк, но возможно придется уменьшить размер шрифта.</p>    
                <p className="attention">В справке значение отступов ограничено от 0 до 50, и настройки шрифта отключены.</p>   
                <Viewport
                    signature='Измените настройки имени проекта.'
                    colorBakground="btn-face"
                    modifier="options"
                >
                    <OptionsAxis
                        axisOptions={nameAxis}
                        nameAxisXHandler={nameAxisHandler('axisX', 'changeName')}
                        nameAxisYHandler={nameAxisHandler('axisY', 'changeName')}
                        offsetTopXHandler={nameAxisHandler('offsetTopAxisX', 'changeOffset')}
                        offsetLeftYHandler={nameAxisHandler('offsetLeftAxisY', 'changeOffset')}
                        isButtonFontDisabled
                    />
                </Viewport>
                <Viewport
                    isStretsh
                    signature='Настройка имени проекта. Зелеными полями отмечены смещение от краев.'
                >
                    <PlotGraph
                        minX={0.01}
                        maxX={10000}
                        minY={0.01}
                        maxY={10000}
                        legend={[]}
                        optionsProject={{
                            ...optionsProject,
                            nameAxis
                        }}
                        isShowHelpers={{ ...isShowHelpers, axis: true }}
                    />
                </Viewport>
            </div>
        </div> 
    );
}

export default DataAxisSection;