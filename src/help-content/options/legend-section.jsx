import React, { useState } from 'react';

import OptionsLegend from '../../components/options-project/options-legend';
import PlotGraph from '../../components/plot-graph';
import Viewport from '../../components/viewport';
import { config } from '../../config';

const DataLegendSection = ({

}) => {
    const { optionsProject } = config;
    const [legend, setLegend] = useState({
        columns: 0,
        height: 0
    });

    const validHeight = v => {
        if (v < 0) return 0;
        if (v > 200) return 200;
        return v;
    }

    const changeLegendOptionsHandler = action => value => {
        const val = action === 'height' ? validHeight(+value.replace(/\D/, "")) : value;
        setLegend({ ...legend, [action]: val });
    }

    return (
        <div className="help">
            <div className="title">Данные окна настроек "Настройка легенды"</div>
            <div className="content">
                <p>Секция "Настройка легенды" отвечает за размеры и вид легенды.</p>
                <p>По умолчанию область легенды делится на равные части для отображения имен характеристик. Через данную секцию можно изменить отображение легенды по своему вкусу.</p>
                <p>Поле "Высота легенды" отвечает за размер занимаемый легендой по высоте. По умолчанию равняется 0, т.е. Гриди-КС автоматически рассчитывает данный параметр.</p>      
                <p className="attention">В справке значение высоты ограничено от 0 до 200, и настройки шрифта отключены.</p>   
                <Viewport
                    signature='Измените настройки имени проекта.'
                    colorBakground="btn-face"
                    modifier="options"
                >
                    <OptionsLegend
                        legendOptions={legend}
                        changeLegendOptionsHandler={changeLegendOptionsHandler}
                        isButtonFontDisabled
                    />
                </Viewport>
                <Viewport
                    isStretsh
                    signature='Настройка легенды. Зеленым полем отмечена высота легенды.'
                >
                    <PlotGraph
                        minX={0.01}
                        maxX={10000}
                        minY={0.01}
                        maxY={10000}
                        legend={[
                            {
                                title: 'Стандартная характеристика',
                                color: '#a80000'
                            },
                            {
                                title: 'Стандартная характеристика',
                                color: '#a80000'
                            },
                            {
                                title: 'Стандартная характеристика',
                                color: '#a80000'
                            },
                            {
                                title: 'Стандартная характеристика',
                                color: '#a80000'
                            },
                            {
                                title: 'Стандартная характеристика',
                                color: '#a80000'
                            },
                            {
                                title: 'Стандартная характеристика',
                                color: '#a80000'
                            },
                            {
                                title: 'Стандартная характеристика',
                                color: '#a80000'
                            },
                            {
                                title: 'Стандартная характеристика',
                                color: '#a80000'
                            },
                            {
                                title: 'Стандартная характеристика',
                                color: '#a80000'
                            }
                        ]}
                        optionsProject={{ ...optionsProject, legend }}
                    />
                </Viewport>
            </div>
        </div> 
    );
}

export default DataLegendSection;