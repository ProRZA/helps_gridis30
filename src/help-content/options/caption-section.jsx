import React, { useState } from 'react';

import OptionsCaption from '../../components/options-project/options-caption';
import PlotGraph from '../../components/plot-graph';
import Viewport from '../../components/viewport';
import NameProject from '../../img/name-project.gif';
import { config } from '../../config';

const DataCaptionSection = () => {
    const { optionsProject, isShowHelpers } = config;
    const [titleOptions, setTitleOptions] = useState(optionsProject.title);

    const validOffset = v => {
        if (v < 0) return 0;
        if (v > 50) return 50;
        return v;
    }

    const changeOptionHandler = action => val => {
        setTitleOptions({
                ...titleOptions,
                [action]: action === 'align' ? val : validOffset(+val)
        })
    }

    return (
        <div className="help">
            <div className="title">Данные окна настроек "Настройка имени проекта"</div>
            <div className="content">
                <p>Секция "Настройка имени проекта" отвечает за позиционирование названия проекта, 
                    которое отображается сверху графика, и по умолчанию имеет текст "Карта селективности".</p>
                <p>Выравнивание заголовка позволяет прижать надпись по левому краю, по центру (по умолчанию) и по правому краю.</p>      
                <p>Отступы по бокам уменьшают область отрисовки текста. 
                    Если текст не помещается в область отрисовки, то он разбивается на несколько строк, но возможно придется уменьшить размер шрифта.</p>    
                <p className="attention">В справке значение отступов ограничено от 0 до 50. Настройки шрифта отключены.</p>   
                <Viewport
                    signature='Измените настройки имени проекта.'
                    colorBakground="btn-face"
                    modifier="options"
                >
                    <OptionsCaption
                        titleOptions={titleOptions}
                        isButtonFontDisabled
                        changeAlignHandler={changeOptionHandler('align')}
                        changeOffsetHandler={changeOptionHandler('offset')}
                    />
                </Viewport>
                <Viewport
                    isStretsh
                    signature='Настройка имени проекта. Зелеными полями отмечены смещение от краев.'
                >
                    <PlotGraph
                        minX={0.01}
                        maxX={10000}
                        minY={0.01}
                        maxY={10000}
                        legend={[]}
                        optionsProject={{ ...optionsProject, title: titleOptions }}
                        isShowHelpers={{ ...isShowHelpers, title: true }}
                    />
                </Viewport>

                <div className="attention">
                    <p>Для того, чтобы изменить текст, откройте окно "Имя проекта" через меню программы "Файл", и далее "Имя проекта".</p>
                    <Viewport
                        signature="Окно для редактирования имени проекта"
                    >
                        <img src={NameProject} />
                    </Viewport>
                </div>
            </div>
        </div> 
    );
}

export default DataCaptionSection;