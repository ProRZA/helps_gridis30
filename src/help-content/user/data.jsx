import React from 'react';

import PlotGraph from '../../components/plot-graph';
import Viewport from '../../components/viewport';

import { config } from '../../config';
import userCurveOne from '../../img/user-curve-one.gif';
import userCurveRegion from '../../img/user-curve-region.gif';

const DataUsert = ({

}) => {
    return (
        <div className="help">
            <div className="title">Данные панели "Пользовательские кривые". Как создать файл пользовательской характеристики.</div>
            <div className="content">
                <p>Файл пользовательской характеристики представляет из себя текстовый файл, в котором находятся значения тока и времени в формате: "ток;время".</p>
                <p>Значения «ток;время» записываются друг за другом. Допускаются пробелы в любом количестве.
                     Разделителем между целыми и дробными частями числа может быть, как точка, так и запятая.</p>
                <p>На каждой новой строке располагается одна пара значений "ток;время". 
                    Таким образом, если характеристика строится по 50 точкам, то в файле должно быть 50 строк.</p>
                <div className="mini_caption gradient_caption">Моделирование характеристики в виде одной кривой.</div>
                <p>Для моделирования одной кривой укажите несколько точек в формате "ток;время".</p>
                <Viewport
                    signature="Данные для построения одной кривой"
                >
                    <img src={userCurveOne} />
                </Viewport>
                <Viewport
                    isStretsh
                    signature="Отдельные пользовательские кривые"
                >
                    <PlotGraph
                        showCurve={["User_line_down", "User_line_up"]}
                        minX={100}
                        maxX={10000}
                        minY={0.01}
                        maxY={10000}
                    />
                </Viewport>
                <div className="mini_caption gradient_caption">Моделирование характеристики в виде области.</div>
                <p>Для моделирования областных характеристик необходимо указать дополнительные ключи: region и #</p>
                <p>Ключ region указывается в начале файла первой строкой, затем идут точки первой и второй кривой разделенные "#".</p>
                <Viewport
                    signature="Данные для построения обласной характеристики"
                >
                    <img src={userCurveRegion} />
                </Viewport>
                <Viewport
                    isStretsh
                    signature="Областная характеристика"
                >
                    <PlotGraph
                        showCurve={["User_line_region"]}
                        minX={100}
                        maxX={10000}
                        minY={0.01}
                        maxY={10000}
                    />
                </Viewport>
                <div className="mini_caption gradient_caption">Ошибки при неверном формате данных.</div>
                <p>Ошибки "Гридис-КС" при неверном формате данных:</p>
                <ul>
                    <li>"Некорректный разделитель" (появляется при отсутствии разделителя ";"" в строке)</li>
                    <li>"Некорректные данные" (появляется в случае, если значения не являются числами, или пропущена парное значение, например, ток, или время)</li>
                </ul>
                <p>Уведомление об ошибке содержится номер строки, в котором ошибка возникла. Пользователь всегда может найти эту строку и отредактировать.</p>
            </div>
        </div> 
    );
}

export default DataUsert;