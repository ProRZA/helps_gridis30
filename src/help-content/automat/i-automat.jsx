import React, { useEffect, useState } from 'react';

import Section from '../../components/panels/automat/section';
import PlotGraph from '../../components/plot-graph';
import Viewport from '../../components/viewport';

import { config } from '../../config';

const IAutomat = ({

}) => {
    const [curve, setCurve] = useState([]);
    const [state, setState] = useState({
        leftTop: {
            label: 'li, о.е.',
            value: '10'
        },
        rightTop: {
            label: 'li, A',
            value: '1000'
        },
        rightBottom: {
            isCheckbox: true,
            off: false,
            onlyOff: true
        }
    });

    useEffect(() => {
        setCurve(state.rightBottom.off ? ["Automat_Son_lioff_independed"] : ["Automat_Son_lion"]);
    }, [state]);

    const changeOff = v => {
        setState({
            ...state,
            rightBottom: {
                ...state.rightBottom,
                off: v
            }
        });
    }

    return (
        <div className="help">
            <div className="title">Данные панели. Секция "КЗ, мгновенно (I)"</div>
            <div className="content">
                <p>Секция "КЗ, мгновенно (I)" отображает и управляет уставками ТО (токовой отсечки) кривой автомата.</p>
                <p>Секция "КЗ, мгновенно (I)" имеет чекбокс OFF. С его помощью можно отключить защитную кривую данной секции. 
                    Отключенная уставка пропадает с области построения, а поля ее блока заполняются в "OFF". 
                    При повторном нажатии на чекбокс "OFF" уставка снова становится активной.
                </p>
                <Viewport
                    modifier="base-panel automat"
                    signature="Автомат Schneider Electric. КЗ, мгновенно (I)"
                >
                    <Section
                        modifier="mlr-5"
                        sectionName="КЗ, мгновенно (I)"
                        inputs={state}
                        changeOffHandler={v => changeOff(v)}
                        disabled={state.rightBottom.off}
                    />
     
                </Viewport>
                <p className="attention">Для просмотра как будет изменяться вид кривой, нажмите чекбокс "OFF".</p>
                <Viewport
                    isStretsh
                    signature="Автомат Schneider Electric. КЗ, мгновенно (I)"
                >
                    <PlotGraph
                        showCurve={curve}
                        minX={10}
                        maxX={10000}
                        minY={0.01}
                        maxY={10000}
                        signatureX={
                            state.rightBottom.off ? 
                            [] :
                            [{ x: 1000, text: 'li', color: config.redBorder }]
                        }
                    />
                </Viewport>
            </div>
            <p className="mt-15">ТО не имеет уставки по времени. По умолчанию уставка по времени равняется 0,01с</p>
        </div> 
    );
}

export default IAutomat;