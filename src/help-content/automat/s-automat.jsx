import React, { useEffect, useState } from 'react';

import Section from '../../components/panels/automat/section';
import PlotGraph from '../../components/plot-graph';
import Viewport from '../../components/viewport';

import { config } from '../../config';

const SAutomat = ({

}) => {
    const [curve, setCurve] = useState([]);
    const [state, setState] = useState({
            leftTop: {
                label: 'Isd, о.е.',
                value: '2,5'
            },
            leftBottom: {
                label: 'Tsd, c',
                value: '0,1'
            },
            rightTop: {
                label: 'Isd, A',
                value: '150'
            },
            rightBottom: {
                isCheckbox: true,
                i2t: false,
                off: false
            }
    });

    useEffect(() => {
        const { off, i2t } = state.rightBottom;
        let c;
        if (off) c = ['Automat_Soff_lion'];
        if (!off && i2t) c = ['Automat_Son_lion_depended'];
        if (!off && !i2t) c = ['Automat_Son_lion'];
        setCurve(c);
    }, [state]);

    const changeI2t = v => {
        setState({
            ...state,
            rightBottom: {
                ...state.rightBottom,
                i2t: v
            }
        });
    }

    const changeOff = v => {
        setState({
            ...state,
            rightBottom: {
                ...state.rightBottom,
                off: v
            }
        });
    }

    return (
        <div className="help">
            <div className="title">Данные панели. Секция "КЗ, с выдержкой времени (S)"</div>
            <div className="content">
                <p>Секция "КЗ, с выдержкой времени (S)" отображает и управляет уставками МТЗ (максимальной токовой защиты) кривой автомата.</p>
                <p>МТЗ может быть как зависимой, так и независимой кривой. С помощью чекбокса I2t можно менять вид кривой.</p>
                <p>Секция "КЗ, с выдержкой времени (S)" имеет чекбокс OFF. С его помощью можно отключить защитную кривую данной секции. 
                    Отключенная уставка пропадает с области построения, а поля ее блока заполняются в "OFF". 
                    При повторном нажатии на чекбокс "OFF" уставка снова становится активной.
                </p>
                <Viewport
                    modifier="base-panel automat"
                    signature="Автомат Schneider Electric. КЗ, с выдержкой времени (S)"
                >
                    <Section
                        modifier="mlr-5"
                        sectionName="КЗ, с выдержкой времени (S)"
                        inputs={state}
                        changeDependedHandler={v => changeI2t(v)}
                        changeOffHandler={v => changeOff(v)}
                        disabled={state.rightBottom.off}
                    />
                </Viewport>
                <p className="attention">Для просмотра как будет изменяться вид кривой, нажмите чекбоксы "I2t" и/или "OFF".</p>
                <Viewport
                    isStretsh
                    signature='Автомат Schneider Electric. Секция "КЗ, с выдержкой времени (S)"'
                >
                    <PlotGraph
                        modifier="flex hc"
                        modifierSVG="shadow"
                        showCurve={curve}
                        minX={10}
                        maxX={10000}
                        minY={0.01}
                        maxY={10000}
                        offsetLeft={60}
                        signatureX={[
                            { x: 150, text: 'Isd', color: config.redBorder },
                        ]}
                        signatureY={[
                            { y: 0.1, text: 'Tsd', color: config.redBorder},
                        ]}
                    /> 
                </Viewport>
                <p className="mt-15">Уставка Isd указывается в относительных единицах и является каталожным значением автомата. 
                    Гридис-КС автоматически пересчитает измененную уставку в амперах. Формулы расчета уставок заложены в автоматы в соответствии с каталожными данными.</p>
                <p>Уставка Tsd указывается в секундах и является выдержкой защиты МТЗ по времени.</p>
            </div>
        </div>
    );
}

export default SAutomat;