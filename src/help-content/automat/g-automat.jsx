import React, { useEffect, useState } from 'react';

import Section from '../../components/panels/automat/section';
import PlotGraph from '../../components/plot-graph';
import Viewport from '../../components/viewport';

import { config } from '../../config';

const GAutomat = () => {
    const [curve, setCurve] = useState([]);
    const [state, setState] = useState({
        leftTop: {
            label: 'Ig, о.е.',
            value: 'A'
        },
        leftBottom: {
            label: 'tg, c',
            value: '0.2'
        },
        rightTop: {
            label: 'Ig, A',
            value: '30'
        },
        rightBottom: {
            isCheckbox: true,
            i2t: false,
            off: false,
        }
    });

    useEffect(() => {
        const { i2t, off } = state.rightBottom;

        let c = ["Automat_Son_lion"];
        if (!off && !i2t) c = ["Automat_Son_lion", "Automat_Gon_independed"];
        if (!off && i2t) c = ["Automat_Son_lion", "Automat_Gon_depended"];
        setCurve(c);
    }, [state]);

    const changeI2t = v => {
        setState({
            ...state,
            rightBottom: {
                ...state.rightBottom,
                i2t: v
            }
        });
    }

    const changeOff = v => {
        setState({
            ...state,
            rightBottom: {
                ...state.rightBottom,
                off: v
            }
        });
    }

    return (
        <div className="help">
            <div className="title">Данные панели. Поле "КЗ на землю (G)"</div>
            <div className="content">
                <p>Секция "КЗ на землю (G)" отображает и управляет уставками ОЗЗ (однофазное замыкание на землю) кривой автомата.</p>
                <p>ОЗЗ может быть как зависимой, так и независимой кривой. С помощью чекбокса I2t можно менять вид кривой.</p>
                <p>Секция "КЗ на землю (G)" имеет чекбокс OFF. С его помощью можно отключить защитную кривую данной секции. 
                    Отключенная уставка пропадает с области построения, а поля ее блока заполняются в "OFF". 
                    При повторном нажатии на чекбокс "OFF" уставка снова становится активной.
                </p>
                <Viewport
                    modifier="base-panel automat mb-15"
                    signature="Автомат Schneider Electric. КЗ на землю (G)"
                >
                    <Section
                        modifier="mlr-5"
                        sectionName="КЗ на землю (G)"
                        inputs={state}
                        changeDependedHandler={v => changeI2t(v)}
                        changeOffHandler={v => changeOff(v)}
                        disabled={state.rightBottom.off}
                    />
                </Viewport>

                <p className="attention">Для просмотра как будет изменяться вид кривой, нажмите чекбоксы I2t и/или OFF.</p>
                <Viewport
                    isStretsh
                    signature='Автомат Schneider Electric. Секция "КЗ на землю (G)"'
                >
                    <PlotGraph
                        modifier="flex hc"
                        modifierSVG="shadow"
                        showCurve={curve}
                        minX={1}
                        maxX={10000}
                        minY={0.01}
                        maxY={10000}
                        signatureX={[
                            { x: 30, text: 'Ig', color: config.redBorder },
                        ]}
                    /> 
                </Viewport>
                <p className="mt-15">Уставка Ig указывается в относительных единицах и является каталожным значением автомата. 
                    Гридис-КС автоматически пересчитает измененную уставку в амперах. Формулы расчета уставок заложены в автоматы в соответствии с каталожными данными.</p>
                <p>Уставка tg указывается в секундах и является выдержкой защиты ОЗЗ по времени.</p>
            </div>
        </div>
    )
};

export default GAutomat;