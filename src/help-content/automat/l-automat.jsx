import React from 'react';

import Section from '../../components/panels/automat/section';
import PlotGraph from '../../components/plot-graph';
import Viewport from '../../components/viewport';

import { config } from '../../config';

const LAutomat = () => (
    <div className="help">
        <div className="title">Данные панели. Секция "Перегрузка (L)"</div>
        <div className="content">
            <p>Секция "Перегрузка (L)" отображает и управляет уставками перегрузочной кривой автомата.</p>
            <p>Перегрузка имеет вид функции времени от тока t=f(i), и показывает сколько времени сможет работать автомат под определенным током.</p>
            <p>Секция "Перегрузка (L)" всегда имеет включенное состояние.</p>
            <Viewport
                modifier="base-panel automat"
                signature="Автомат Schneider Electric. Перегрузка (L)"
            >
                <Section
                    modifier="mlr-5 automat-section"
                    sectionName="Перегрузка (L)"
                    inputs={{
                        leftTop: {
                            label: 'Ir, о.е.',
                            value: '0,6'
                        },
                        leftBottom: {
                            label: 'Tr, c',
                            value: '1'
                        },
                        rightTop: {
                            label: 'Ir, A',
                            value: '60'
                        },
                        rightBottom: {
                            label: 'При 6 x Ir',
                            isStatic: true
                        }
                    }}
                />
            </Viewport>
            <p className="mt-15">Уставка Ir указывается в относительных единицах и является каталожным значением данного автомата. 
                Гридис-КС автоматически пересчитает уставку в амперах. Формулы расчета уставок заложены в автоматы в соответствии с каталожными данными.</p>
            <p>Уставка Tr указывается в секундах.</p>
            <p>Ir и Tr указывают точку, через которую пройдет защитная кривая перегрузки автомата при токе 6Ir.</p>
            <p className="attention">Не всегда график проходит через 6Ir. Эта цифра характеризует кривую Schneider Electirc Micrologic. У иных производителей краность Ir может различаться.</p>
            <p className="attention">На графике ниже показана пунктирная кривая, которая является продолжением графика до точки 6Ir.</p>
            <Viewport
                signature='Автомат Schneider Electric. Секция "Перегрузка (L)"'
                isStretsh
            >
                <PlotGraph
                    showCurve={["Automat_Son_lion", "Automat_Son_lion_L"]}
                    minX={10}
                    maxX={10000}
                    minY={0.01}
                    maxY={10000}
                    offsetLeft={50}
                    signatureX={[
                        { x: 60, text: 'Ir', color: config.redBorder },
                        { x: 360, text: '6Ir=360A', color: config.greenBorder },
                    ]}
                    signatureY={[
                        { y: 1, text: 'Tr', color: config.greenBorder},
                    ]}
                /> 
            </Viewport>
        </div>
    </div>
);

export default LAutomat;