import React from 'react';

import Input from '../../components/input';
import PlotGraph from '../../components/plot-graph';
import Viewport from '../../components/viewport';

import { config } from '../../config';

const INomAutomat = () => (
    <div className="help">
        <div className="title">Данные панели. Поле "Номинальный ток автомата"</div>
        <div className="content">
            <span>Данное поле задает номинальный ток автомата в амперах.</span>
            <Viewport
                signature="Поле для ввода In"
            >
                <Input
                    modifier="flex vc hc pd-10"
                    inputModifier="ml-5 w150px"
                    labelText="In, A"
                    labelPosition="left"
                    value="100"
                    iconType="spin"
                />
            </Viewport>
            <p><b>Номинальный ток</b> — наибольший допустимый по условиям нагрева токопроводящих частей и изоляции ток, при котором 
            оборудование может работать неограниченно длительное время.</p>

            <p>Уставка In является базовой для остальных уставок.</p>
            <Viewport
                isStretsh
                signature="Автомат Schneider Electric. In, A"
            >
                <PlotGraph
                    modifier="flex hc"
                    modifierSVG="shadow"
                    showCurve={["Automat_Son_lion"]}
                    minX={10}
                    maxX={10000}
                    minY={0.01}
                    maxY={10000}
                    signatureX={[
                        { x: 100, text: 'In', color: config.redBorder },
                    ]}
                /> 
            </Viewport>
        </div>
    </div>
);

export default INomAutomat;