import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import './style/style.sass';
import Header from './components/header';
import Footer from './components/footer';
import HomePage from './pages/home';
import StandartPage from './pages/standart';
import AutomatPage from './pages/automat';
import TempAutomatPage from './pages/temp-automat';
import ModulePage from './pages/module';
import FusePage from './pages/fuse';
import RelayPage from './pages/relay';
import UserPage from './pages/user';
import OptionsProjectPage from './pages/options-project';
import Dev from './pages/dev';

import { ScreenContext } from './context';

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isMobile: window.matchMedia(`(max-width: 767px)`).matches
        };
        this.routs = [
            { page: 'standart', сomponent: () => <StandartPage/> },
            { page: 'automat', сomponent: () => <AutomatPage/> },
            { page: 'temp-automat', сomponent: () => <TempAutomatPage/> },
            { page: 'module', сomponent: () => <ModulePage/> },
            { page: 'fuse', сomponent: () => <FusePage/> },
            { page: 'relay', сomponent: () => <RelayPage/> },
            { page: 'user-curve', сomponent: () => <UserPage/> },
            { page: 'options-project', сomponent: () => <OptionsProjectPage /> },
            { page: 'dev', сomponent: () => <Dev /> },
            { page: '', сomponent: () => <HomePage/> },
        ]
    
    }

    componentDidMount() {
        window.addEventListener('resize', () => {
            this.setState({ isMobile: window.matchMedia(`(max-width: 767px)`).matches });
        });
    }

    componentWillUnmount(){
        window.removeEventListener('resize');
    }

    render() {
        return (
            <div className="wrapper-page" style={{ position: 'relative' }}>
                <ScreenContext.Provider value={{ isMobile: window.matchMedia(`(max-width: 767px)`).matches }}>
                    <Header />
                        <Router>
                            <Switch>
                                {this.routs.map((route, index) => {
                                    const { page, сomponent } = route;
                                    return (
                                        <Route key={index} path={`${process.env.SITE_PATH}${page}`}>
                                            {сomponent}
                                        </Route>
                                    )
                                })}
                            </Switch>
                        </Router> 
                    <Footer />
                </ScreenContext.Provider>
            </div>
        );
    }
}

export default App;
